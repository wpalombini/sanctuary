﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.IO;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Sanctuary.Models;
using System.Text;
using Autofac;
using Sanctuary.IoC;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Authorization;
using Sanctuary.Components.Filters;
using Sanctuary.Domain.Helpers;
using Sanctuary.Domain.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Sanctuary
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new RequireHttpsAttribute());
            });

            // Add framework services.
            services.AddMvc();

            //setup Authentication
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            string domain = $"https://wpalombini.au.auth0.com/";
            string audience = $"https://wpalombini.au.auth0.com/api/v2/";

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = tokenValidationParameters;
                options.Authority = domain;
                options.Audience = audience;
            });

            //setup Authorization
            services.AddAuthorization(options =>
            {
                options.AddPolicy(Global.CustomAuthorization,
                    policy => policy.AddRequirements(new CustomAuthorizationRequirement()));
            });
            services.AddScoped<IAuthorizationHandler, CustomAuthorizationHandler>();

            // Add CORS
            services.AddCors();

            // Create the Autofac container builder.
            var builder = new ContainerBuilder();

            // Add any Autofac modules or registrations.
            builder.RegisterModule(new AutoFacModule());
            
            // Populate the services.
            builder.Populate(services);
            
            // Build the container.
            this.ApplicationContainer = builder.Build();
            
            // Create and return the service provider.
            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetService<Repositories.Context.DataContext>().Database.Migrate();
            }

            var options = new RewriteOptions()
                        .AddRedirectToHttps();

            app.UseRewriter(options);

            app.UseCors(builder =>
                builder.WithOrigins("http://localhost:8080", "https://localhost:8080")
                       .AllowAnyHeader()
                       .AllowAnyMethod()
                );

            app.Use(async (context, next) =>
            {
                if (context.Request.Path.Value.EndsWith("creds.json"))
                {
                    context.Response.StatusCode = 404;
                    return;
                }

                await next();
                

                if (context.Response.StatusCode == 404
                && (context.Request.Path.HasValue && context.Request.Path.Value.Contains("/api/") == false)
                && Path.HasExtension(context.Request.Path.Value) == false)
                {
                    context.Request.Path = "/index.html";
                    await next();
                }

            });

            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = 500;

                    var error = context.Features.Get<IExceptionHandlerFeature>();

                    if (error != null)
                    {
                        var ex = error.Error;

                        var errorDto = new ErrorDto();
                        if (ex is BaseException)
                        {
                            errorDto.Message = ex.Message;
                            errorDto.InnerException = ex.InnerException?.ToString();
                            errorDto.StackTrace = ex.StackTrace;
                        }
                        else
                        {
                            errorDto.Message = "A general error occurred.";
                        }

                        await context.Response.WriteAsync(errorDto.ToString(), Encoding.UTF8);
                    }
                });
            });

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc();

            
        }
    }
}
