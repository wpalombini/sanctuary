﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

import { AppRoutingModule } from './app.routing.module';

import { BaseDataServiceInterceptor } from './shared/services/base.data.service.interceptor';

import { NavigationService } from './shared/services/navigation.service';
import { TokenService } from './shared/services/token.service';

import { AnimationHelper } from './shared/helpers/animation.helper';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/components/menu.component';
import { HomeComponent } from './home/components/home.component';
import { ContactComponent } from './contact/components/contact.component';
import { FooterComponent } from './footer/components/footer.component';

import { StoreModule } from '@ngrx/store';
import * as Reducers from 'ngrx';
import { LoaderComponent } from 'loader/components/loader.component';
import { EffectsModule } from '@ngrx/effects';
import { AuthenticationService } from 'account/services/authentication.service';
import { AuthenticationDataService } from 'account/services/authentication.data.service';

@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,

        StoreModule.forRoot(Reducers.reducers),
        EffectsModule.forRoot([])
    ],
    declarations: [
        AppComponent,
        LoaderComponent,
        MenuComponent,
        HomeComponent,
        ContactComponent,
        FooterComponent
    ],
    entryComponents: [
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: BaseDataServiceInterceptor,
            multi: true
        },
        AuthenticationService,
        AuthenticationDataService,
        NavigationService,
        TokenService,
        AnimationHelper,
        Subject
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }