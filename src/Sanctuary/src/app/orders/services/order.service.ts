import { Injectable } from "@angular/core";
import { OrderDataService } from "orders/services/order.data.service";
import { AnimationHelper } from "shared/helpers/animation.helper";
import { OrderModel } from "orders/models/order.model";
import { Observable } from "rxjs/Observable";
import { _throw } from "rxjs/observable/throw";
import { Constants } from "shared/constants";

@Injectable()
export class OrderService {

    private readonly baseOrdersUrl: string = `${Constants.basePaymentsApiUrl}/orders`;

    constructor(
        private dataService: OrderDataService,
        private animationHelper: AnimationHelper) { }

    
    GetOrdersForAnimalId(animalId: string): Observable<OrderModel[]> {

        this.animationHelper.startLoading();

        let orderModel = new OrderModel();
        orderModel.animalId = animalId;

        return this.dataService.HttpGet<OrderModel, any>(this.baseOrdersUrl, orderModel)
            .map(result => {
                return result.data;
            })
            .catch((err) => {
                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }
}