export class OrderModel {
    Id: string;
    animalId: string;
    createdAt: Date;
    totalAmount: number;
}