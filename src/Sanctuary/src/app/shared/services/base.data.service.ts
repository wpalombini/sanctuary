﻿import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { TokenService } from '../services/token.service';
import { NavigationService } from 'shared/services/navigation.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Store } from '@ngrx/store';
import { AccountLoggedOut } from 'account/ngrx/actions/account.actions';
import { AccountState } from 'account/ngrx/reducers';
import { _throw } from 'rxjs/observable/throw';


export abstract class BaseDataService {

    private readonly headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    constructor(
        private _http: HttpClient,
        private _tokenService: TokenService,
        private _navigationService: NavigationService,
        private _store: Store<AccountState>
    ) {

    }

    HttpGet<T, U>(url: string, data?: T): Observable<U> {

        let params = this.appendParams(new HttpParams(), data);

        return this._http.get<U>(url, { headers: this.headers, params: params })
            .catch((err: HttpErrorResponse) => {
                let errorMessage: string = this.handleError(err);
                return _throw(errorMessage);
            });
    }

    HttpPost<T, U>(url: string, data: T): Observable<U> {
        return this._http.post<U>(url, data, { headers: this.headers })
            .catch((err: HttpErrorResponse) => {
                let errorMessage: string = this.handleError(err);
                return _throw(errorMessage);
            });
    }

    protected appendParams(params: HttpParams, obj: any) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                params = params.append(key, obj[key])
            }
        }
        return params;
    }

    protected handleError(error: HttpErrorResponse | any) {

        let errMsg: string;
        if (error instanceof HttpErrorResponse) {
            if (error.status == 401) {
                this._tokenService.Delete();
                this._store.dispatch(new AccountLoggedOut());
                this._navigationService.navigateTo('/account/login');
                return;
            }

            if (error.error != null) {
                return error.error.Message;
            } else {
                return "A general error occurred while processing your request. Please try again later.";
            }

        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        return errMsg;
    }
}