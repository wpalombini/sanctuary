﻿import { Injectable } from '@angular/core';
import { TokenModel } from '../models/token.model';

@Injectable()
export class TokenService {

    private readonly storageName: string = 'token-data';

    constructor() {

    }

    Get(): TokenModel {

        return <TokenModel>JSON.parse(localStorage.getItem(this.storageName));

    }

    Save(token: TokenModel): void {

        this.Delete();

        localStorage.setItem(this.storageName, JSON.stringify(token));

    }

    Delete(): void {

        localStorage.removeItem(this.storageName);

    }
}