﻿import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { TokenService } from './token.service';

@Injectable()
export class BaseDataServiceInterceptor implements HttpInterceptor {
    constructor(private tokenService: TokenService) {

    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let token = this.tokenService.Get();
        
        if (token != null) {

            request = request.clone({
                setHeaders: {
                    Authorization: `${token.tokenType} ${token.accessToken}`
                }
            });

        }
        
        return next.handle(request);
    }
}