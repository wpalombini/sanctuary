﻿import { Injectable } from '@angular/core';

import { Store } from '@ngrx/store';
import * as LoaderActions from 'loader/ngrx/actions/loader.actions';
import { AppState } from 'ngrx';

@Injectable()
export class AnimationHelper {

    constructor(private store: Store<AppState>) { }

    startLoading() {
        this.store.dispatch(new LoaderActions.StartLoading());
    }

    endLoading() {
        this.store.dispatch(new LoaderActions.EndLoading());
    }

}