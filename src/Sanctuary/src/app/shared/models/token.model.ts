﻿export class TokenModel {
    tokenValue: string;
    expiredAt: Date;

    accessToken: string;
    expiresIn: number;
    idToken: string;
    scope: string;
    tokenType: string;
}