﻿export class Constants {
    static readonly currentApiVersion: string = 'v1';
    static readonly baseApiUrl: string = `https://localhost:44378/api/${Constants.currentApiVersion}`;
    static readonly basePaymentsApiUrl: string = `http://localhost:3000/${Constants.currentApiVersion}`;
}