import { CanActivate,
        ActivatedRouteSnapshot,
        RouterStateSnapshot,
        CanActivateChild,
        CanLoad,
        Route, 
        Router} from "@angular/router";
import { TokenService } from "shared/services/token.service";
import { Injectable } from "@angular/core";

@Injectable()
export class AuthenticationGuard implements CanActivate, CanActivateChild, CanLoad {

    constructor(
        private tokenService: TokenService,
        private router: Router) {

    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.handleRoute();
    }

    canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.handleRoute();
    }

    canLoad(route: Route): boolean {
        return this.handleRoute();
    }

    private handleRoute() {
        
        if (this.isLoggedIn()) {
            return true;
        } else {
            this.router.navigate(['account/login']);
            return false;
        }
    }

    private isLoggedIn(): boolean {
        return this.tokenService.Get() != null;
    }
}