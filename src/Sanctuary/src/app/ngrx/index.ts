import { ActionReducerMap } from "@ngrx/store";
import { LoaderState, LoaderReducer } from "loader/ngrx/reducers/loader.reducer";
import { AccountState } from "account/ngrx/reducers";
import * as accountReducers from 'account/ngrx/reducers/account.reducers';
import * as animalReducers from 'animal/ngrx/reducers/animal.reducers';
import { AnimalState } from "animal/ngrx/reducers";

export interface AppState {
    LoaderState: LoaderState,
    AccountState: AccountState,
    AnimalState: AnimalState
 }

export const reducers: ActionReducerMap<AppState> = {
    LoaderState: LoaderReducer.loaderReducer,
    AccountState: accountReducers.AccountReducer.accountReducer,
    AnimalState: animalReducers.AnimalReducer.animalReducer
};