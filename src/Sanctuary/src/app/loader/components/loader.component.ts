﻿import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { AppState } from 'ngrx';

@Component({
    selector: 'loader-component',
    templateUrl: './loader.component.html'
})
export class LoaderComponent implements OnDestroy {

    public showLoader: boolean = false;    
    private subscriptions: Subscription[] = new Array<Subscription>();

    constructor(private store: Store<AppState>) {

        this.subscriptions.push(this.store.select(state => state.LoaderState)
            .subscribe(data => {
                if (data.isLoading == true)
                    this.showLoader = true;
                else if (data.isLoading == false)
                    setTimeout(() => {
                        this.showLoader = false;
                    }, 500);
            })
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }
}