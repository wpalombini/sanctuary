import { Action } from '@ngrx/store';

export const START_LOADING = 'START_LOADING';
export const END_LOADING = 'END_LOADING';

export class StartLoading implements Action {
    readonly type = START_LOADING;
}

export class EndLoading implements Action {
    readonly type = END_LOADING;
}

export type LoadActions = StartLoading | EndLoading;