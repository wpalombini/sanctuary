import { Action } from '@ngrx/store';

import * as LoaderActions from 'loader/ngrx/actions/loader.actions';

export interface LoaderState {
    isLoading: boolean
}

const initialState: LoaderState = {
    isLoading: false
};

export class LoaderReducer {

    static loaderReducer(state = initialState, action: LoaderActions.LoadActions): LoaderState {
        switch (action.type) {
            case LoaderActions.START_LOADING: {

                state = Object.assign({}, state, { isLoading: true });

                return state;
            }
            case LoaderActions.END_LOADING: {
                
                state = Object.assign({}, state, { isLoading: false });

                return state;
            }
            default: {
                return state;
            }
        }
    }
}