﻿export class ContactModel {
    emailAddress: string;
    name: string;
    message: string;
}