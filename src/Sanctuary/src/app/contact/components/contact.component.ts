﻿import { Component } from '@angular/core';
import { ContactDataService } from 'contact/services/contact.data.service';
import { ContactService } from 'contact/services/contact.service';

@Component({
    selector: 'contact-component',
    templateUrl: './contact.component.html',
    providers: [
        ContactDataService,
        ContactService
    ]
})
export class ContactComponent {
    
    constructor() {
        
    }
}