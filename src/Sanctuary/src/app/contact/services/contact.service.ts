﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ContactDataService } from './contact.data.service';
import { AnimationHelper } from 'shared/helpers/animation.helper';
import { ContactModel } from '../models/contact.model';
import { Constants } from 'shared/constants';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/finally';
import { _throw } from 'rxjs/observable/throw';

@Injectable()
export class ContactService {

    private baseContactUrl = `${Constants.baseApiUrl}/contact/`;

    constructor(private contactDataService: ContactDataService,
                private animationHelper: AnimationHelper) {

    }

    PostMessage(message: ContactModel): Observable<ContactModel> {

        this.animationHelper.startLoading();

        return this.contactDataService.HttpPost<ContactModel, ContactModel>(this.baseContactUrl + 'postmessage', message)
            .catch((err) => {

                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }

}