﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { SanctuaryModel } from 'sanctuary/models/sanctuary.model';
import { SanctuaryDataService } from './sanctuary.data.service';
import { AnimationHelper } from 'shared/helpers/animation.helper';
import { Constants } from 'shared/constants';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/finally';
import { _throw } from 'rxjs/observable/throw';

@Injectable()
export class SanctuaryService {
    private readonly baseSanctuaryUrl: string = `${Constants.baseApiUrl}/sanctuary/`;

    constructor(
        private dataService: SanctuaryDataService,
        private animationHelper: AnimationHelper) { console.log('sanctuary service loaded') }

    CreateSanctuary(sanctuaryModel: SanctuaryModel): Observable<SanctuaryModel> {

        this.animationHelper.startLoading();

        return this.dataService.HttpPost<SanctuaryModel, SanctuaryModel>(this.baseSanctuaryUrl + 'create', sanctuaryModel)
            .catch((err) => {
                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }

    EditSanctuary(sanctuaryModel: SanctuaryModel): Observable<SanctuaryModel> {
        this.animationHelper.startLoading();

        return this.dataService.HttpPost<SanctuaryModel, SanctuaryModel>(this.baseSanctuaryUrl + 'edit', sanctuaryModel)
            .catch((err) => {
                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }

    GetSanctuariesForAccountId(): Observable<SanctuaryModel> {

        this.animationHelper.startLoading();

        return this.dataService.HttpGet<null, SanctuaryModel>(this.baseSanctuaryUrl + 'GetSanctuariesForAccountId')
            .catch((err) => {
                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }
}