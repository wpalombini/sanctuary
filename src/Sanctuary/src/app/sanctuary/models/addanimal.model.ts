﻿export class AddAnimalModel {
    name: string;
    gender: string;
    sanctuaryId: string;
    animalSpeciesId: string;
    animalBreedId: string;
    age: number;
}