﻿export class SanctuaryModel {
    id: string;
    name: string;
    fullAddress: string;
    createdAt: Date;
    deletedAt: Date;
}