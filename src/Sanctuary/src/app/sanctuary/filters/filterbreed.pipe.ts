﻿import { Pipe, PipeTransform } from '@angular/core';
import { AnimalBreedModel } from 'animal/models/animalbreed.model';

@Pipe({
    name: 'filterBreed'
})
export class FilterBreedPipe implements PipeTransform {

    transform(breedList: Array<AnimalBreedModel>, selectedSpeciesId: string) {

        if (breedList != null && selectedSpeciesId != null) {
            breedList = breedList.filter(f => f.animalSpeciesId == selectedSpeciesId);
        }

        return breedList;
    }
}