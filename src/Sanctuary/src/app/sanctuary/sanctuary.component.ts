﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { SanctuaryModel } from 'sanctuary/models/sanctuary.model';
import { SanctuaryService } from 'sanctuary/services/sanctuary.service';

@Component({
    templateUrl: './sanctuary.component.html'
})
export class SanctuaryComponent implements OnInit, OnDestroy {

    private subscriptions: Subscription[] = new Array<Subscription>();
    public sanctuaryModel: SanctuaryModel = new SanctuaryModel();
    constructor(
        private sanctuaryService: SanctuaryService) {

    }

    ngOnInit() {
        
        this.subscriptions.push(this.sanctuaryService.GetSanctuariesForAccountId()
            .subscribe(data => {
                this.sanctuaryModel = data[0];
            },
            err => {

            })
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subs) => subs.unsubscribe());
    }
}