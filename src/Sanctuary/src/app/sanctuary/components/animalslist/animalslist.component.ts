﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AnimalModel } from 'animal/models/animal.model';
import { AnimalFilterModel } from 'animal/models/animalfilter.model';
import { SpeciesAndBreedModel } from 'animal/models/speciesandbreed.model';
import { AnimalService } from 'animal/services/animal.service';
import { SanctuaryService } from 'sanctuary/services/sanctuary.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    templateUrl: './animalslist.component.html',
    styleUrls: [
        './animalslist.component.css'
    ]
})
export class AnimalsListComponent implements OnInit, OnDestroy {

    public model: AnimalModel[] = new Array<AnimalModel>();
    public filterModel: AnimalFilterModel = new AnimalFilterModel();
    public speciesAndBreedsModel: SpeciesAndBreedModel = new SpeciesAndBreedModel();
    public errorMessage: string;
    public successMessage: string;
    private subscriptions: Subscription[] = new Array<Subscription>();

    constructor(
        private activatedRoute: ActivatedRoute,
        private animalService: AnimalService,
        private sanctuaryService: SanctuaryService,
        private router: Router
    ) {

    }

    ngOnInit() {
        
        this.subscriptions.push(this.activatedRoute.queryParams
            .subscribe(queryParam => {
                this.filterModel.animalBreedId = queryParam['animalBreedId'] || "";
                this.filterModel.animalSpeciesId = queryParam['animalSpeciesId'] || "";
                this.filterModel.animalName = queryParam['animalName'] || "";
                this.filterModel.page = queryParam['page'] || 1;
                this.filterModel.resultsPerPage = queryParam['resultsPerPage'] || 5;

                if (this.filterModel.sanctuaryId != null && this.filterModel.sanctuaryId.length > 0) {

                    this.subscriptions.push(this.animalService.GetAnimals(this.filterModel)
                        .subscribe(
                        data => {
                            this.model = data;
                        },
                        err => {
                            this.errorMessage = err;
                        })
                    );

                } else {

                    this.subscriptions.push(this.sanctuaryService.GetSanctuariesForAccountId()
                        .subscribe(data => {
                            this.filterModel.sanctuaryId = data[0].id;

                            this.subscriptions.push(this.animalService.GetAnimals(this.filterModel)
                                .subscribe(
                                data => {
                                    this.model = data;
                                },
                                err => {
                                    this.errorMessage = err;
                                })
                            );
                        },
                        err => {
                            this.errorMessage = err;
                        })
                    );
                }

            })
        );

        this.subscriptions.push(this.animalService.GetSpeciesAndBreeds()
            .subscribe(data => {
                this.speciesAndBreedsModel = data;
            },
            err => {
                console.log(err);
            })
        );
    }

    applyFilter() {
        
        let navigationExtras: NavigationExtras = {
            queryParams: this.filterModel
            //fragment: 'anchor' //navigate to a specific anchor
        };

        this.router.navigate(['sanctuary'], navigationExtras);
    }

    resetFilter() {
        let navigationExtras: NavigationExtras = {
            queryParams: this.filterModel = new AnimalFilterModel()
            //fragment: 'anchor' //navigate to a specific anchor
        };

        this.router.navigate(['sanctuary'], navigationExtras);
    }

    onAnimalSpeciesChanged() {
        this.filterModel.animalBreedId == null;
    }

    onAnimalClick(animalId: string) {
        let navigationExtras: NavigationExtras = {
            queryParams: { id: animalId }
            //fragment: 'anchor' //navigate to a specific anchor
        };
        this.router.navigate(['sanctuary/animal/details'], navigationExtras);
    }

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}