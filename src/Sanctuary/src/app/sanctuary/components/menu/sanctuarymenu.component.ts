﻿import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { SanctuaryModel } from 'sanctuary/models/sanctuary.model';


@Component({
    selector: 'sanctuarymenu-component',
    templateUrl: './sanctuarymenu.component.html'
})
export class SanctuaryMenuComponent implements OnInit, OnDestroy {

    @Input() sanctuaryModel: SanctuaryModel

    constructor() {

    }

    ngOnInit() {

    }

    ngOnDestroy() {

    }
}