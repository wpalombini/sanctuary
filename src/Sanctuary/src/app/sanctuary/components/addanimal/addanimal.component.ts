﻿import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AddAnimalModel } from 'sanctuary/models/addanimal.model';
import { SpeciesAndBreedModel } from 'animal/models/speciesandbreed.model';
import { AnimalService } from 'animal/services/animal.service';
import { SanctuaryService } from 'sanctuary/services/sanctuary.service';

@Component({
    templateUrl: './addanimal.component.html'
})
export class AddAnimalComponent implements OnInit, OnDestroy {

    public model: AddAnimalModel = new AddAnimalModel();
    public speciesAndBreedsModel: SpeciesAndBreedModel = new SpeciesAndBreedModel();
    public errorMessage: string;
    public successMessage: string;
    private subscriptions: Subscription[] = new Array<Subscription>();

    constructor(
        private animalService: AnimalService,
        private sanctuaryService: SanctuaryService) {

    }

    ngOnInit() {
        this.subscriptions.push(this.animalService.GetSpeciesAndBreeds()
            .subscribe(data => {
                this.speciesAndBreedsModel = data;
            },
            err => {
                console.log(err);
            })
        );
        
        this.subscriptions.push(this.sanctuaryService.GetSanctuariesForAccountId()
            .subscribe(data => {
                this.model.sanctuaryId = data[0].id;
            },
            err => {

            })
        );
    }

    onFormSubmit(form: any) {
        this.successMessage = null;
        this.errorMessage = null;

        this.subscriptions.push(this.animalService.Create(this.model)
            .subscribe(data => {
                this.model = data;
                this.successMessage = `${data.name} has been successfully registered!`;
                form.form.reset();
            },
            err => {
                this.errorMessage = err;
            })
        );
    }

    onAnimalSpeciesChanged(form: any) {
        this.model.animalBreedId = null;
        form.controls['selectedAnimalBreedId'].markAsPristine();
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subs) => subs.unsubscribe());
    }
}