﻿import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Subject, empty } from 'rxjs';

import { AddAnimalComponent } from './addanimal.component';
import { AnimalService } from 'animal/services/animal.service';
import { AnimalDataService } from 'animal/services/animal.data.service';
import { SanctuaryDataService } from 'sanctuary/services/sanctuary.data.service';
import { SanctuaryService } from 'sanctuary/services/sanctuary.service';
import { NavigationService } from 'shared/services/navigation.service';
import { TokenService } from 'shared/services/token.service';
import { FilterBreedPipe } from 'sanctuary/filters/filterbreed.pipe';

import { AnimationHelper } from 'shared/helpers/animation.helper';
import { StoreModule } from '@ngrx/store';
import * as Reducers from 'ngrx';

describe('AddAnimalComponent', () => {
    let component: AddAnimalComponent;
    let fixture: ComponentFixture<AddAnimalComponent>;

    beforeEach(() => {

        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                HttpClientModule,
                RouterTestingModule,
                StoreModule.forRoot(Reducers.reducers)
            ],
            declarations: [
                AddAnimalComponent,
                FilterBreedPipe
            ],
            providers: [
                AnimalService,
                SanctuaryService,
                SanctuaryDataService,
                AnimalDataService,
                TokenService,
                NavigationService,
                Subject,
                AnimationHelper
            ]
        });

        fixture = TestBed.createComponent(AddAnimalComponent);
        component = fixture.componentInstance;

        let animalService = TestBed.get(AnimalService);
        let sanctuaryService = TestBed.get(SanctuaryService);

        spyOn(animalService, 'GetSpeciesAndBreeds')
            .and.returnValue(empty());

        spyOn(sanctuaryService, 'GetSanctuariesForAccountId')
            .and.returnValue(empty());

    });

    it('should create an instance', () => {

        fixture.detectChanges();

        expect(component).toBeTruthy();
    });

    it('should disable register button when required fields are invalid', () => {

    })

});