import { NgModule } from "@angular/core";
import { SanctuaryComponent } from "sanctuary/sanctuary.component";
import { AddAnimalComponent } from "sanctuary/components/addanimal/addanimal.component";
import { AnimalsListComponent } from "sanctuary/components/animalslist/animalslist.component";
import { SanctuaryMenuComponent } from "sanctuary/components/menu/sanctuarymenu.component";
import { FilterBreedPipe } from "sanctuary/filters/filterbreed.pipe";
import { SanctuaryService } from "sanctuary/services/sanctuary.service";
import { SanctuaryDataService } from "sanctuary/services/sanctuary.data.service";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { AnimalService } from "animal/services/animal.service";
import { AnimalDataService } from "animal/services/animal.data.service";

const sanctuaryRoutes: Routes = [
    { 
        path: '',
        component: SanctuaryComponent,
        children: [
            { path: 'addanimal', component: AddAnimalComponent },
            { path: 'animals', component: AnimalsListComponent },
            {
                path: 'animal',
                loadChildren: '../animal/animal.module#AnimalModule'
            },
            { path: '', component: AnimalsListComponent }
        ]
    }
];


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(sanctuaryRoutes)
    ],
    declarations: [
        //Components
        SanctuaryComponent,
        AddAnimalComponent,
        AnimalsListComponent,
        SanctuaryMenuComponent,

        //Pipes
        FilterBreedPipe
    ],
    providers: [
        SanctuaryService,
        SanctuaryDataService,
        AnimalService,
        AnimalDataService
    ],
    exports: [
        
    ]
})
export class SanctuaryModule { }