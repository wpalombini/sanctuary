﻿import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { TokenService } from '../../shared/services/token.service';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { AppState } from 'ngrx';

@Component({
    selector: 'menu-component',
    templateUrl: './menu.component.html',
    styleUrls: [
        './menu.component.css'
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent implements OnInit, OnDestroy {

    private subscriptions: Subscription[] = new Array<Subscription>();

    public isLoggedIn: boolean;

    constructor(
        private tokenService: TokenService,
        private store: Store<AppState>,
        private changeDetector: ChangeDetectorRef) {

    }

    onMenuLinkClicked() {
        let chkBox = (<HTMLInputElement>document.getElementById("menuHiddenChkBox"));
        chkBox.checked = !chkBox.checked;
    }

    ngOnInit(): void {

        this.isLoggedIn = this.tokenService.Get() != null;

        this.subscriptions.push(this.store.select(state => state.AccountState)
            .subscribe(data => {
                if (data.accountCreated) {
                    this.isLoggedIn = true;
                } else if (data.accountLoggedIn) {
                    this.isLoggedIn = true;
                } else if (data.accountLoggedOut) {
                    this.isLoggedIn = false;
                }

                this.changeDetector.detectChanges();
            })
        );
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }
}