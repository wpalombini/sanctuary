import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { Store } from "@ngrx/store";
import { AppState } from "ngrx";
import { AnimalLoading } from "animal/ngrx/actions/animal.actions";
import { AnimalState } from "animal/ngrx/reducers";
import { AnimalModel } from "animal/models/animal.model";
import { skip } from "rxjs/operators/skip";
import { Location } from "@angular/common";
import { OrderService } from "orders/services/order.service";
import { OrderModel } from "orders/models/order.model";

@Component({
    templateUrl: './animal-details.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnimalDetailsComponent implements OnInit, OnDestroy {

    model: AnimalModel;
    ordersModel: OrderModel[]
    private subscriptions: Subscription[] = new Array<Subscription>();

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private store: Store<AppState>,
        private changeDetector: ChangeDetectorRef,
        private location: Location,
        private orderService: OrderService
    ) {

    }

    ngOnInit() {
        
        this.subscriptions.push(
            this.store.select((state: AppState) => state.AnimalState)
                .pipe(
                    skip(1) //skip initial state
                )
                .subscribe((animalState: AnimalState) => {
                    if (animalState.animalLoaded != null) {
                        this.model = animalState.animalLoaded;
                        this.changeDetector.detectChanges();
                    }
                })
        );

        this.subscriptions.push(
            this.activatedRoute.queryParams
                .subscribe(queryParam => {
                    let animalId = queryParam['id'] || null;
                    this.loadAnimal(animalId);
                    this.loadOrders(animalId);
                })
        );
    }

    private loadAnimal(animalId: string) {
        if (!animalId) {
            this.router.navigate(['sanctuary']);
        } else {
            this.store.dispatch(new AnimalLoading(animalId));
        }
    }

    private loadOrders(animalId: string) {
        this.subscriptions.push(
            this.orderService.GetOrdersForAnimalId(animalId)
            .subscribe(result => {
                this.ordersModel = result;
                this.changeDetector.detectChanges();
            })
        );
    }

    goBack() {
        this.location.back();
    }

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}