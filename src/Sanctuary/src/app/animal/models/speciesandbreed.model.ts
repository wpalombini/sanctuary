﻿import { AnimalSpeciesModel } from './animalspecies.model';
import { AnimalBreedModel } from './animalbreed.model';

export class SpeciesAndBreedModel {
    animalSpecies: Array<AnimalSpeciesModel>;
    animalBreeds: Array<AnimalBreedModel>
}