﻿export class AnimalBreedModel {
    id: string;
    name: string;
    animalSpeciesId: string;
}