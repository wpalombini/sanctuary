﻿export class AnimalModel {
    id: string;
    name: string;
    animalBreedName: string;
    animalSpeciesName: string;
}