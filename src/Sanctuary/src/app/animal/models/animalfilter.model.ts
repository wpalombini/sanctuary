﻿export class AnimalFilterModel {
    sanctuaryId: string = "";
    animalSpeciesId: string = "";
    animalBreedId: string = "";
    animalName: string = "";
    resultsPerPage: number = 5;
    page: number = 1;
}