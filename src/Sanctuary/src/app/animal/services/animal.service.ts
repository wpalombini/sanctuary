﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AddAnimalModel } from 'sanctuary/models/addanimal.model';
import { SpeciesAndBreedModel } from 'animal/models/speciesandbreed.model';
import { AnimalFilterModel } from 'animal/models/animalfilter.model';
import { AnimalModel } from 'animal/models/animal.model';
import { AnimalDataService } from './animal.data.service';
import { AnimationHelper } from 'shared/helpers/animation.helper';
import { Constants } from 'shared/constants';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/finally';
import { _throw } from 'rxjs/observable/throw';

@Injectable()
export class AnimalService {
    private readonly baseAnimalUrl: string = `${Constants.baseApiUrl}/animal/`;

    constructor(
        private dataService: AnimalDataService,
        private animationHelper: AnimationHelper) { }

    GetSpeciesAndBreeds(): Observable<SpeciesAndBreedModel> {

        this.animationHelper.startLoading();

        return this.dataService.HttpGet<null, SpeciesAndBreedModel>(this.baseAnimalUrl + 'GetSpeciesAndBreeds')
            .catch((err) => {
                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }

    Create(addAnimalModel: AddAnimalModel): Observable<AddAnimalModel> {

        this.animationHelper.startLoading();

        return this.dataService.HttpPost<AddAnimalModel, AddAnimalModel>(this.baseAnimalUrl + 'Create', addAnimalModel)
            .catch((err) => {
                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }

    GetAnimals(filter: AnimalFilterModel): Observable<AnimalModel[]> {

        this.animationHelper.startLoading();

        return this.dataService.HttpGet<AnimalFilterModel, AnimalModel[]>(this.baseAnimalUrl + 'getanimals', filter)
            .catch((err) => {
                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }

    GetAnimal(animalId: string): Observable<AnimalModel> {

        this.animationHelper.startLoading();

        let animal = new AnimalModel();
        animal.id = animalId;

        return this.dataService.HttpGet<AnimalModel, AnimalModel>(this.baseAnimalUrl + 'getanimalbyid', animal)
            .catch((err) => {
                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }

}