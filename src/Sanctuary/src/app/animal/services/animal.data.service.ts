﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseDataService } from 'shared/services/base.data.service';
import { NavigationService } from 'shared/services/navigation.service';
import { TokenService } from 'shared/services/token.service';
import { Store } from '@ngrx/store';
import { AccountState } from 'account/ngrx/reducers';

@Injectable()
export class AnimalDataService extends BaseDataService {
    constructor(
        private http: HttpClient,
        private tokenService: TokenService,
        private navigationService: NavigationService,
        private store: Store<AccountState>
    ) {
        super(http, tokenService, navigationService, store);
    }
}