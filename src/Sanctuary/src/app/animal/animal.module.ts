import { NgModule } from "@angular/core";
import { AnimalService } from "animal/services/animal.service";
import { AnimalDataService } from "animal/services/animal.data.service";
import { Routes, RouterModule } from "@angular/router";
import { AnimalContainerComponent } from "./components/animal-container.component";
import { AnimalDetailsComponent } from "./components/details/animal-details.component";
import { EffectsModule } from "@ngrx/effects";
import { AnimalEffects } from "./ngrx/effects/animal.effects";
import { CommonModule } from "@angular/common";
import { OrderService } from "orders/services/order.service";
import { OrderDataService } from "orders/services/order.data.service";

const animalRoutes: Routes = [
    { 
        path: '',
        component: AnimalContainerComponent,
        children: [
            {
                path: '',
                component: AnimalDetailsComponent
            },
            {
                path: 'details',
                component: AnimalDetailsComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(animalRoutes),
        EffectsModule.forFeature([AnimalEffects])
    ],
    declarations: [
        AnimalContainerComponent,
        AnimalDetailsComponent
    ],
    providers: [
        AnimalService,
        AnimalDataService,
        OrderService,
        OrderDataService
    ],
    exports: [
        
    ]
})
export class AnimalModule { }