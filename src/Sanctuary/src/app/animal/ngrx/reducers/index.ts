import { AnimalModel } from "animal/models/animal.model";

export interface AnimalState {
    animalLoading: string;
    animalLoaded: AnimalModel
}