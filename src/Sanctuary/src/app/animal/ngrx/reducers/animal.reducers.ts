import { AnimalState } from ".";
import * as AnimalActions from 'animal/ngrx/actions/animal.actions';

const initialState: AnimalState = {
    animalLoading: null,
    animalLoaded: null
}

export class AnimalReducer {
    static animalReducer(state = initialState, action: AnimalActions.AnimalActions): AnimalState {
        switch (action.type) {
            case AnimalActions.ANIMAL_LOADING:
                state = Object.assign({}, {...initialState});
                state.animalLoading = (action as AnimalActions.AnimalLoading).payload;
                return state;
            
            case AnimalActions.ANIMAL_LOADED:
                state = Object.assign({}, {...initialState});
                state.animalLoaded = (action as AnimalActions.AnimalLoaded).payload;
                return state;
            default:
                return state;
        }
    }
}