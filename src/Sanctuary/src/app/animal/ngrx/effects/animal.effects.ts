import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { AnimalService } from "animal/services/animal.service";
import { AnimalLoading, ANIMAL_LOADING, AnimalLoaded } from "../actions/animal.actions";
import { map } from "rxjs/operators/map";
import { switchMap } from "rxjs/operators/switchMap";
import { AnimalModel } from "animal/models/animal.model";
import { catchError } from "rxjs/operators";
import { of } from "rxjs/observable/of";

@Injectable()
export class AnimalEffects {

    constructor(
        private actions$: Actions,
        private animalService: AnimalService
    ) { }

    @Effect() AnimalLoading$ = this.actions$
    .pipe(
        ofType<AnimalLoading>(ANIMAL_LOADING),
        map(action => action.payload),
        switchMap((animalId: string) => {
            return this.animalService.GetAnimal(animalId)
                .pipe(
                    map((animalModel: AnimalModel) => {
                        return new AnimalLoaded(animalModel);
                    }),
                    catchError(err => {
                        return of(err);
                    })
                )
        })
    )
}

