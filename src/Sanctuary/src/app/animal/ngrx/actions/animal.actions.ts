import { Action } from "@ngrx/store";
import { AnimalModel } from "animal/models/animal.model";

export const ANIMAL_LOADING = 'ANIMAL_LOADING';
export const ANIMAL_LOADED = 'ANIMAL_LOADED';

export class AnimalLoading implements Action {
    type = ANIMAL_LOADING;
    constructor(public payload: string) { }
}

export class AnimalLoaded implements Action {
    type = ANIMAL_LOADED;
    constructor(public payload: AnimalModel) { }
}

export type AnimalActions = AnimalLoading
                            | AnimalLoaded;