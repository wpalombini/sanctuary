import { NgModule } from "@angular/core";
import { AccountDetailsComponent } from "account/components/accountdetails/accountdetails.component";
import { LoginComponent } from "account/components/login/login.component";
import { LogoutComponent } from "account/components/logout/logout.component";
import { RequestNewPassword } from "account/components/requestnewpassword/requestnewpassword.component";
import { SignUpComponent } from "account/components/signup/signup.component";
import { AccountService } from "account/services/account.service";
import { AccountDataService } from "account/services/account.data.service";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { SanctuaryService } from "sanctuary/services/sanctuary.service";
import { SanctuaryDataService } from "sanctuary/services/sanctuary.data.service";
import { Routes, RouterModule } from "@angular/router";
import { EffectsModule } from "@ngrx/effects";
import { AccountEffects } from "./ngrx/effects/account.effects";
import { AuthenticationGuard } from "shared/guard/authentication.guard";


const accountRoutes: Routes = [
    {
        path: 'signup',
        component: SignUpComponent
    },
    {
        path: 'requestnewpassword',
        component: RequestNewPassword
    },
    {
        path: 'logout',
        component: LogoutComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: '',
        component: AccountDetailsComponent,
        canActivate: [AuthenticationGuard]
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(accountRoutes),
        EffectsModule.forFeature([AccountEffects])

    ],
    declarations: [
        AccountDetailsComponent,
        LoginComponent,
        LogoutComponent,
        RequestNewPassword,
        SignUpComponent
    ],
    providers: [
        AccountService,
        AccountDataService,
        SanctuaryService,
        SanctuaryDataService,
        AuthenticationGuard
    ],
    exports: [
        
    ]
})
export class AccountModule { }