﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Constants } from 'shared/constants';
import { Store } from '@ngrx/store';
import { AccountLoggedOut, AccountLoggingOut } from 'account/ngrx/actions/account.actions';
import { NavigationService } from 'shared/services/navigation.service';
import { Subscription } from 'rxjs/Subscription';
import { AppState } from 'ngrx';

@Component({
    selector: 'logout-component',
    template: ``
})
export class LogoutComponent implements OnInit, OnDestroy {

    private subscriptions: Subscription[] = new Array<Subscription>();

    constructor(
                private navigationService: NavigationService,
                private store: Store<AppState>) {

    }

    ngOnInit(): void {
        this.store.dispatch(new AccountLoggingOut());

        this.subscriptions.push(this.store.select(state => state.AccountState)
            .subscribe(data => {
                this.navigationService.navigateTo('/');
            })
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subs) => subs.unsubscribe());
    }
}