﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { TokenService } from 'shared/services/token.service';
import { NavigationService } from 'shared/services/navigation.service';
import { SignUpModel } from 'account/models/signup.model';
import { Store } from '@ngrx/store';
import { AccountCreated, AccountSignUpFailed, AccountSigningUp, AccountLoggingIn } from 'account/ngrx/actions/account.actions';
import { LoginModel } from 'account/models/login.model';
import { AppState } from 'ngrx';


@Component({
    templateUrl: './signup.component.html'
})
export class SignUpComponent implements OnInit, OnDestroy {

    public model: SignUpModel = new SignUpModel();
    private subscriptions: Subscription[] = new Array<Subscription>();
    public errorMessage: string;

    constructor(
        private tokenService: TokenService,
        private navigationService: NavigationService,
        private store: Store<AppState>) {

    }

    onFormSubmit(form: any) {

        this.store.dispatch(new AccountSigningUp(this.model));

        this.subscriptions.push(this.store.select(state => state.AccountState)
            .subscribe(data => {
                if (data.accountCreated) {
                    //Account successfully created. Login now.
                    let loginModel = new LoginModel();
                    loginModel.emailAddress = this.model.emailAddress;
                    loginModel.password = this.model.password;

                    this.store.dispatch(new AccountLoggingIn(loginModel));

                }
                else if (data.accountSignUpFailed) {
                    this.errorMessage = data.accountSignUpFailed.message;
                }
                // else if (data.accountCreatingFailed) {
                    // TODO: Roll back account created within the Authentication Provider 
                // }
                else if (data.accountLoggedIn) {
                    //Account successfully logged in. Redirect.
                    this.errorMessage = null;
                    form.form.reset();
                    this.navigationService.navigateTo('/account');

                }
                else if (data.accountLoginFailed) {
                    this.errorMessage = data.accountLoginFailed.message;
                }
            })
        );
    }

    ngOnInit() {

        if (this.tokenService.Get() != null) {
            this.navigationService.navigateTo('/account');
        }
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subs) => subs.unsubscribe());
    }
}