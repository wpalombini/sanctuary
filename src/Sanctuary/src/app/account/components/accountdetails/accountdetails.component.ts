﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AccountService } from 'account/services/account.service';
import { SanctuaryService } from 'sanctuary/services/sanctuary.service';
import { AccountModel } from 'account/models/account.model';
import { ChangePasswordModel } from 'account/models/changepassword.model';
import { SanctuaryModel } from 'sanctuary/models/sanctuary.model';
import { Store } from '@ngrx/store';
import { AccountChangingPassword } from 'account/ngrx/actions/account.actions';
import { AppState } from 'ngrx';

@Component({
    templateUrl: './accountdetails.component.html'
})
export class AccountDetailsComponent implements OnInit, OnDestroy {

    private subscriptions: Subscription[] = new Array<Subscription>();

    public accountModel: AccountModel;
    public changePasswordModel: ChangePasswordModel = new ChangePasswordModel();
    public sanctuaryDetailsModel: SanctuaryModel = new SanctuaryModel();

    public errorMessage: string;
    public changePasswordErrorMessage: string;
    public contactDetailsErrorMessage: string;
    public sanctuaryDetailsErrorMessage: string;

    public changePasswordSuccessMessage: string;
    public contactDetailsSuccessMessage: string;
    public sanctuaryDetailsSuccessMessage: string;

    public sanctuaryEditMode: boolean = false;

    constructor(
        private store: Store<AppState>,
        private accountService: AccountService,
        private sanctuaryService: SanctuaryService) {

    }

    onAddSanctuaryFormSubmit(form: any) {
        this.sanctuaryDetailsErrorMessage = null;
        this.sanctuaryDetailsSuccessMessage = null;

        this.subscriptions.push(this.sanctuaryService.CreateSanctuary(this.sanctuaryDetailsModel)
            .subscribe(data => {
                this.sanctuaryDetailsModel = data;
                this.sanctuaryDetailsSuccessMessage = "Sanctuary successfully registered!";
                this.sanctuaryEditMode = true;
            },
            err => {
                this.sanctuaryDetailsErrorMessage = err;
            })
        );

        form.form.markAsPristine();
    }

    onEditSanctuaryFormSubmit(form: any) {
        this.sanctuaryDetailsErrorMessage = null;
        this.sanctuaryDetailsSuccessMessage = null;

        this.subscriptions.push(this.sanctuaryService.EditSanctuary(this.sanctuaryDetailsModel)
            .subscribe(data => {
                this.sanctuaryDetailsModel = data;
                this.sanctuaryDetailsSuccessMessage = "Sanctuary successfully updated!";
            },
            err => {
                this.sanctuaryDetailsErrorMessage = err;
            })
        );

        form.form.markAsPristine();
    }

    oncontactDetailsFormSubmit(form: any) {
        this.contactDetailsSuccessMessage = null;
        this.contactDetailsErrorMessage = null;

        this.subscriptions.push(this.accountService.EditContactDetails(this.accountModel)
            .subscribe(data => {
                this.accountModel = data;
                this.contactDetailsSuccessMessage = "Contact details successfully updated!";
            },
            err => {
                this.contactDetailsErrorMessage = err;
            })
        );

        form.form.markAsPristine();
    }

    onchangePasswordFormSubmit(form: any) {
        this.changePasswordSuccessMessage = null;
        this.changePasswordErrorMessage = null;

        if (this.changePasswordModel.password.length < 6) {
            this.changePasswordErrorMessage = "the new password must have at least 6 characters or numbers.";
            return;
        }

        if (this.changePasswordModel.password != this.changePasswordModel.confirmedPassword) {
            this.changePasswordErrorMessage = "passwords don't match!";
            return;
        }

        this.store.dispatch(new AccountChangingPassword(this.changePasswordModel));

        this.subscriptions.push(this.store.select(state => state.AccountState)
            .subscribe(data => {
                if (data.accountChangedPassword) {
                    this.changePasswordSuccessMessage = "Password successfully updated!";
                    this.changePasswordErrorMessage = null;
                    form.form.reset();
                }
                else if (data.accountChangePasswordFailed) {
                    this.changePasswordErrorMessage = data.accountChangePasswordFailed.message;
                }
            })
        )
    }

    ngOnInit() {
        
        this.subscriptions.push(this.sanctuaryService.GetSanctuariesForAccountId()
            .subscribe(data => {
                this.sanctuaryDetailsModel = data[0];
                this.sanctuaryEditMode = true;
            },
            err => {
                this.sanctuaryEditMode = false;
            })
        );
        

        this.subscriptions.push(this.accountService.GetAccountDetails()
            .subscribe(data => {
                this.accountModel = data;
            },
            err => {
                this.errorMessage = err;
            })
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subs) => subs.unsubscribe());
    }
}