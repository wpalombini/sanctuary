﻿import { fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Subject, empty } from 'rxjs';

import { AccountDetailsComponent } from './accountdetails.component';
import { AccountService } from 'account/services/account.service';
import { SanctuaryDataService } from 'sanctuary/services/sanctuary.data.service';
import { SanctuaryService } from 'sanctuary/services/sanctuary.service';
import { TokenService } from 'shared/services/token.service';
import { NavigationService } from 'shared/services/navigation.service';
import { AccountDataService } from 'account/services/account.data.service';
import { AnimationHelper } from 'shared/helpers/animation.helper';
import { StoreModule } from '@ngrx/store';
import * as Reducers from 'ngrx';
import { AuthenticationService } from 'account/services/authentication.service';
import { AuthenticationDataService } from 'account/services/authentication.data.service';

describe('AccountDetailsComponent', () => {

    let component: AccountDetailsComponent;
    let fixture: ComponentFixture<AccountDetailsComponent>;

    /*  This async call with the compileComponents() is not needed as we are using webpack and all the files are bundled together.
        This is used for reading and compiling the physical typescript files.*/
    //beforeEach(async(() => {
    //    TestBed.configureTestingModule({
    //        declarations: [AccountDetailsComponent]
    //    })
    //        .compileComponents();
    //}));

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                HttpClientModule,
                RouterTestingModule,
                StoreModule.forRoot(Reducers.reducers)
            ],
            declarations: [AccountDetailsComponent],
            providers: [
                AccountService,
                SanctuaryService,
                SanctuaryDataService,
                AccountDataService,
                AuthenticationService,
                AuthenticationDataService,
                TokenService,
                NavigationService,
                Subject,
                AnimationHelper
            ]
        });

        fixture = TestBed.createComponent(AccountDetailsComponent);
        component = fixture.componentInstance;

        let accountService = TestBed.get(AccountService);
        let sanctuaryService = TestBed.get(SanctuaryService);

        spyOn(accountService, 'GetAccountDetails')
            .and.returnValue(empty());

        spyOn(sanctuaryService, 'GetSanctuariesForAccountId')
            .and.returnValue(empty());

        /*
        this line runs the ngOnInit, executing model bindings, etc
        so we should call it on each "it";
        */
        //fixture.detectChanges();
    });

    it('should create an instance', () => {
        
        fixture.detectChanges();

        expect(component).toBeTruthy();
    });

    it('should display error message if new password is shorter than 5 characters', fakeAsync(() => {
        //Arrange
        let passwordEl: DebugElement = fixture.debugElement.query(By.css('#input-password'));
        let confirmedPasswordEl: DebugElement = fixture.debugElement.query(By.css('#input-confirmed-password'));
        let formEl: DebugElement = passwordEl.parent.parent;
        let btnSubmit: DebugElement = formEl.query(By.css('button[type="submit"]'));

        fixture.detectChanges();
        tick();

        //Act
        passwordEl.nativeElement.value = "test";
        confirmedPasswordEl.nativeElement.value = "test";
        passwordEl.nativeElement.dispatchEvent(new Event('input'));
        confirmedPasswordEl.nativeElement.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        tick();
        
        console.log('1 - btn is disabled: ' + btnSubmit.nativeElement.disabled);
        btnSubmit.nativeElement.click();
        tick();
        
        fixture.detectChanges();
        //tick();
        console.log('2 - btn is disabled: ' + btnSubmit.nativeElement.disabled);
        console.log('2 - model.password: ' + component.changePasswordModel.password);
        
        let errMsgEl: DebugElement = formEl.query(By.css('.alert-danger span'));

        //Assert
        expect(errMsgEl.nativeElement.textContent).toBeTruthy();
        expect(errMsgEl.nativeElement.textContent).toBe(component.changePasswordErrorMessage);
    }));

    it('should have the save button disabled if any field is empty on the change password form', () => {

    });

    it('should set variable editMode of Sanctuary if existing Sanctuaries are found', () => {

    });

    it('should display the edit sanctuary form if there is existing sanctuary data', () => {

    });
});