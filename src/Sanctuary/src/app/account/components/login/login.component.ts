﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { LoginModel } from 'account/models/login.model';
import { NavigationService } from 'shared/services/navigation.service';
import { TokenService } from 'shared/services/token.service';
import { Constants } from 'shared/constants';
import { Store } from '@ngrx/store';
import { AccountCreated, AccountLoggedIn, AccountLoggingIn } from 'account/ngrx/actions/account.actions';
import { AppState } from 'ngrx';

@Component({
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {

    public errorMessage: string;
    public model: LoginModel = new LoginModel();
    private subscriptions: Subscription[] = new Array<Subscription>();

    constructor(
        private tokenService: TokenService,
        private navigationService: NavigationService,
        private store: Store<AppState>) {

    }

    onFormSubmit(form: any) {

        this.store.dispatch(new AccountLoggingIn(this.model));

        this.subscriptions.push(this.store.select(state => state.AccountState)
            .subscribe(data => {
                if (data.accountLoggedIn) {
                    this.errorMessage = null;
                    form.form.reset();
                    this.navigationService.navigateTo('/sanctuary');
                }
                else if (data.accountLoginFailed) {
                    this.errorMessage = data.accountLoginFailed.message;
                }
            })
        );
    }

    ngOnInit() {
        if (this.tokenService.Get() != null) {
            this.navigationService.navigateTo('/sanctuary');
        }
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subs) => subs.unsubscribe());
    }
}