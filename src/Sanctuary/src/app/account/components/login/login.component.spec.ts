﻿import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, Subject } from 'rxjs';

import { LoginComponent } from './login.component';
import { TokenService } from 'shared/services/token.service';
import { NavigationService } from 'shared/services/navigation.service';
import { AccountDataService } from 'account/services/account.data.service';
import { LoginModel } from 'account/models/login.model';
import { AnimationHelper } from 'shared/helpers/animation.helper';
import * as Reducers from 'ngrx';
import { StoreModule, Store } from '@ngrx/store';
import { AccountState } from 'account/ngrx/reducers';
import { EffectsModule } from '@ngrx/effects';
import { AccountEffects } from 'account/ngrx/effects/account.effects';
import { AccountService } from 'account/services/account.service';
import { AuthenticationService } from 'account/services/authentication.service';
import { AuthenticationDataService } from 'account/services/authentication.data.service';

describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                HttpClientModule,
                RouterTestingModule,
                StoreModule.forRoot(Reducers.reducers),
                EffectsModule.forRoot([AccountEffects])
            ],
            declarations: [LoginComponent],
            providers: [
                AccountService,
                AccountDataService,
                AuthenticationService,
                AuthenticationDataService,
                TokenService,
                NavigationService,
                Subject,
                AnimationHelper
            ]
        });

        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
    });

    it('should have called ngOnInit', () => {
        //Arrange
        //let accountService = fixture.debugElement.injector.get(AccountService);

        //Act
        // spyOn(accountService, 'RequestLogin')
        //     .and.returnValue(Observable.empty);

        var ngOnInitSpy = spyOn(component, 'ngOnInit');

        fixture.detectChanges();

        //Assert
        expect(ngOnInitSpy).toHaveBeenCalled();
    });

    it('should have called ngOnDestroy', () => {
        //Arrange
        //let accountService = fixture.debugElement.injector.get(AccountService);
        let store = fixture.debugElement.injector.get(Store);

        //Act
        // spyOn(store, 'select')
        //     .and.returnValue(Observable.empty);

        var ngOnDestroySpy = spyOn(component, 'ngOnDestroy');
        
        fixture.detectChanges();
        fixture.destroy();
        
        //Assert
        expect(ngOnDestroySpy).toHaveBeenCalled();
    });

    // it('should return an error message if email or password are invalid', () => {
    //     //Arrange
    //     let store = fixture.debugElement.injector.get(Store);
    //     let errorMessage = "Invalid email address or password";
    //     let emailEl = fixture.debugElement.query(By.css('input[name="emailAddress"]')).nativeElement;
    //     let passwordEl = fixture.debugElement.query(By.css('input[name="password"]')).nativeElement;
    //     let btnEl = fixture.debugElement.query(By.css('button[type="submit"]')).nativeElement;
    //     let loginModel = new LoginModel();
    //     loginModel.message = errorMessage;
    //     let accountState: AccountState = {
    //         accountSigningUp: null,
    //         accountSignUpFailed: null,
    //         accountCreated: false,
    //         accountLoggingIn: null,
    //         accountLoginFailed: loginModel,
    //         accountLoggedIn: false,
    //         accountLoggingOut: false,
    //         accountLoggedOut: false,
    //         accountRequestingNewPassword: null,
    //         accountRequestedNewPassword: false
    //     };
    //     //Act
    //     emailEl = "testing@component.net";
    //     passwordEl = "mypassword";

    //     // spyOn(store, 'dispatch')
    //     //     .and.callThrough();
        

    //     // spyOn(store, 'select')
    //     //     .and.returnValue(Observable.throw(accountState));
        
    //     btnEl.click();

    //     fixture.detectChanges();

    //     //this element only exists if there is an error message
    //     let errMsgEl = fixture.debugElement.query(By.css('div.alert-danger span')).nativeElement;

    //     //Assert
    //     expect(errMsgEl.textContent).toBe(errorMessage);
    // });
});