﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RequestNewPasswordModel } from 'account/models/requestnewpassword.model';
import { AccountService } from 'account/services/account.service';
import { Store } from '@ngrx/store';
import { AccountRequestingNewPassword } from 'account/ngrx/actions/account.actions';
import { AppState } from 'ngrx';

@Component({
    templateUrl: './requestnewpassword.component.html'
})
export class RequestNewPassword implements OnInit, OnDestroy {

    private subscriptions: Subscription[] = new Array<Subscription>();
    public errorMessage: string;
    public successMessage: string;
    public model: RequestNewPasswordModel = new RequestNewPasswordModel();

    constructor(private accountService: AccountService, private store: Store<AppState>) {

    }

    onFormSubmit(form: any) {

        this.store.dispatch(new AccountRequestingNewPassword(this.model));

        this.subscriptions.push(this.store.select(state => state.AccountState)
            .subscribe(data => {
                this.successMessage = `Please check your email for instructions on how to change your password.`;
            },
            err => {
                this.errorMessage = err;
            })
        );
    }

    ngOnInit() {
        this.successMessage = null;
        this.errorMessage = null;
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subs) => subs.unsubscribe());
    }
}