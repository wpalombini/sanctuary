import { Injectable } from "@angular/core";
import { BaseDataService } from "shared/services/base.data.service";
import { HttpClient } from "@angular/common/http";
import { TokenService } from "shared/services/token.service";
import { NavigationService } from "shared/services/navigation.service";
import { AccountState } from 'account/ngrx/reducers';
import { Store } from "@ngrx/store";



@Injectable()
export class AuthenticationDataService extends BaseDataService {

    constructor(
        private http: HttpClient,
        private tokenService: TokenService,
        private navigationService: NavigationService,
        private store: Store<AccountState>
    ) {
        super(http, tokenService, navigationService, store);
    }
}