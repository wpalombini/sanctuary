import { Injectable } from "@angular/core";
import * as auth0 from 'auth0-js';
import { LoginModel } from "account/models/login.model";
import { TokenService } from "shared/services/token.service";
import { TokenModel } from "shared/models/token.model";
import { Observable } from "rxjs/Observable";
import { AnimationHelper } from "shared/helpers/animation.helper";
import { Observer } from "rxjs/Observer";
import { SignUpModel } from "account/models/signup.model";
import { RequestNewPasswordModel } from "account/models/requestnewpassword.model";
import { ChangePasswordModel } from "account/models/changepassword.model";
import { Constants } from "shared/constants";
import { AuthenticationDataService } from "account/services/authentication.data.service";


@Injectable()
export class AuthenticationService {

    auth0 = new auth0.WebAuth({
        clientID: 'zfmmXYYU2Oyy0F1tbWLYqmihpYit7y16',
        domain: 'wpalombini.au.auth0.com',
        audience: 'https://wpalombini.au.auth0.com/api/v2/',
        responseType: 'token id_token',
        scope: 'openid',
        leeway: 60
    });

    private readonly baseAccountUrl: string = `${Constants.baseApiUrl}/authentication/`;

    constructor(private tokenService: TokenService,
                private authenticationDataService: AuthenticationDataService,
                private animationHelper: AnimationHelper) {

    }

    SignUp(signUpModel: SignUpModel): Observable<SignUpModel> {

        this.animationHelper.startLoading();

        return Observable.create((observer: Observer<SignUpModel>) => {
            this.auth0.signup({
                email: signUpModel.emailAddress,
                password: signUpModel.password,
                connection: 'Username-Password-Authentication-Development'
            }, (err, signUpResult) => {
                if (err) {
                    observer.error(err.description);
                }
                else if (signUpResult) {
                    signUpModel.authenticationDetails = `auth0|${signUpResult.Id}`;
                    observer.next(signUpModel);
                }

                this.animationHelper.endLoading();
                    
                observer.complete();
            });
        })
    }

    Login(loginModel: LoginModel): Observable<TokenModel> {

        this.animationHelper.startLoading();

        return Observable.create((observer: Observer<TokenModel>) => {
            this.auth0.client.login({
                realm: 'Username-Password-Authentication-Development',
                username: loginModel.emailAddress,
                password: loginModel.password
                }, (err, authResult) => {
                    if (err) {
                        observer.error(err.description);
                    }
                    else if (authResult && authResult.accessToken && authResult.idToken) {
                        let tokenModel = new TokenModel();
                        tokenModel.accessToken = authResult.accessToken;
                        tokenModel.expiresIn = authResult.expiresIn;
                        tokenModel.idToken = authResult.idToken;
                        tokenModel.tokenType = authResult.tokenType;

                        observer.next(tokenModel);
                    }

                    this.animationHelper.endLoading();
                    
                    observer.complete();
            });

        });
    }

    Logout() {
        this.tokenService.Delete();
    }

    RequestNewPassword(requestNewPasswordModel: RequestNewPasswordModel): Observable<RequestNewPasswordModel> {
        
        this.animationHelper.startLoading();

        return Observable.create((observer: Observer<RequestNewPasswordModel>) => {
            this.auth0.changePassword({
                connection: 'Username-Password-Authentication-Development',
                email: requestNewPasswordModel.emailAddress
            }, (err, result) => {
                console.log(err);
                console.log(result);
                if (err) {
                    requestNewPasswordModel.message = `An error occurred while processing your request.`;
                    observer.error(requestNewPasswordModel);
                }
                else if (result) {
                    observer.next(requestNewPasswordModel);
                }

                this.animationHelper.endLoading();

                observer.complete();
            })
        })
    }

    ChangePassword(model: ChangePasswordModel): Observable<boolean> {
        
        this.animationHelper.startLoading();

        return this.authenticationDataService.HttpPost<ChangePasswordModel, boolean>(this.baseAccountUrl + 'changepassword', model)
            .catch((err) => {
                return Observable.throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }
}