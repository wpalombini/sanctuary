﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AccountDataService } from './account.data.service';
import { LoginModel } from '../models/login.model';
import { AccountModel } from '../models/account.model';
import { SignUpModel } from '../models/signup.model';
import { AnimationHelper } from 'shared/helpers/animation.helper';
import { Constants } from 'shared/constants';
import { _throw } from "rxjs/observable/throw";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/finally';

@Injectable()
export class AccountService {

    private readonly baseAccountUrl: string = `${Constants.baseApiUrl}/account/`;

    constructor(private dataService: AccountDataService,
                private animationHelper: AnimationHelper) { }

    GetAccountDetails(): Observable<AccountModel> {

        this.animationHelper.startLoading();

        return this.dataService.HttpGet<null, AccountModel>(this.baseAccountUrl)
            .catch((err) => {
                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }

    EditContactDetails(accountModel: AccountModel): Observable<AccountModel> {

        this.animationHelper.startLoading();

        return this.dataService.HttpPost<AccountModel, AccountModel>(this.baseAccountUrl + 'editcontactdetails', accountModel)
            .catch((err) => {
                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }

    CreateAccount(signUpModel: SignUpModel): Observable<LoginModel> {

        this.animationHelper.startLoading();

        return this.dataService.HttpPost<SignUpModel, AccountModel>(this.baseAccountUrl + 'create', signUpModel)
            .map(accountModel => {
                let loginModel = new LoginModel();
                loginModel.emailAddress = signUpModel.emailAddress;
                loginModel.password = signUpModel.password;
                return loginModel;
            })
            .catch((err) => {
                return _throw(err);
            })
            .finally(() => {
                this.animationHelper.endLoading();
            });
    }
    
}