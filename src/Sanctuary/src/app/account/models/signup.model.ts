﻿export class SignUpModel {
    authenticationDetails: string;
    emailAddress: string;
    password: string;
    message: string;
    tokenValue: string;
    expiredAt: Date;
}