﻿export class ChangePasswordModel {
    password: string;
    confirmedPassword: string;
    success: boolean;
    message: string;
}