﻿export class AccountModel {
    id: string;
    emailAddress: string;
    createdAt: Date;
    firstName: string;
    lastName: string;
}