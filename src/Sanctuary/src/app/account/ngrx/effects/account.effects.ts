import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { ACCOUNT_LOGGING_IN, AccountLoggedIn, AccountLoginFailed, ACCOUNT_SIGNING_UP, AccountCreated, AccountSignUpFailed, AccountLoggingIn, ACCOUNT_CREATING, AccountCreating, AccountSigningUp, AccountLoggedOut, ACCOUNT_LOGGED_OUT, AccountLoggingOut, ACCOUNT_LOGGING_OUT, AccountRequestingNewPassword, ACCOUNT_REQUESTING_NEW_PASSWORD, AccountRequestedNewPassword, AccountChangingPassword, ACCOUNT_CHANGING_PASSWORD, AccountChangedPassword, AccountChangePasswordFailed } from "account/ngrx/actions/account.actions";
import { AccountService } from "account/services/account.service";
import { TokenService } from "shared/services/token.service";
import { LoginModel } from "account/models/login.model";
import { SignUpModel } from "account/models/signup.model";
import { AuthenticationService } from "account/services/authentication.service";

//Importing them separately in order to reduce bundle size
import { map, switchMap, catchError } from "rxjs/operators";
import { ChangePasswordModel } from "account/models/changepassword.model";
import { of } from "rxjs/observable/of";


@Injectable()
export class AccountEffects {

    constructor(private actions$: Actions,
                private accountService: AccountService,
                private authenticationService: AuthenticationService,
                private tokenService: TokenService) { }

    @Effect() AccountSigningUp$ = this.actions$
    .pipe(
        ofType<AccountSigningUp>(ACCOUNT_SIGNING_UP),
        map(action => action.payload),
        switchMap(signUpModel => this.authenticationService.SignUp(signUpModel)
            .pipe(
                map(signUpModel => {
                    return new AccountCreating(signUpModel);
                }),
                catchError(err => {
                    let signUpModel = new SignUpModel();
                    if (err.rules) {
                        signUpModel.message = `Minimum of 6 characters in length.`;
                    }
                    else {
                        signUpModel.message = err;
                    }
                    return of(new AccountSignUpFailed(signUpModel))
                })
            )
        )
    );

    @Effect() AccountCreating$ = this.actions$
    .pipe(
        ofType<AccountCreating>(ACCOUNT_CREATING),
        map(action => action.payload),
        switchMap(signUpModel => this.accountService.CreateAccount(signUpModel)
            .pipe(
                map(loginModel => {
                    return new AccountCreated();
                }),
                catchError(err => {
                    let signUpModel = new SignUpModel();
                    signUpModel.message = err;
                    return of(new AccountSignUpFailed(signUpModel))
                })
            )
        )
    );
    
    @Effect() AccountLoggingIn$ = this.actions$
    .pipe(
        ofType<AccountLoggingIn>(ACCOUNT_LOGGING_IN),
        map(action => action.payload),
        switchMap(loginModel => this.authenticationService.Login(loginModel)
            .pipe(
                map(tokenModel => {
                    this.tokenService.Save(tokenModel);
                    return new AccountLoggedIn();
                }),
                catchError(err => {
                    let loginModel = new LoginModel();
                    loginModel.message = err;
                    return of(new AccountLoginFailed(loginModel))
                })
            )
        )
    );

    @Effect() AccountLoggingOut$ = this.actions$
    .pipe(
        ofType<AccountLoggingOut>(ACCOUNT_LOGGING_OUT),
        map(() => {
            this.tokenService.Delete();
            return new AccountLoggedOut();
        })
    );

    @Effect() AccountRequestingNewPassword$ = this.actions$
    .pipe(
        ofType<AccountRequestingNewPassword>(ACCOUNT_REQUESTING_NEW_PASSWORD),
        map(action => action.payload),
        switchMap(requestNewPasswordModel => this.authenticationService.RequestNewPassword(requestNewPasswordModel)
            .pipe(
                map(data => new AccountRequestedNewPassword()),
                catchError(err => {
                    return of(err);
                })
            )
        )
    );

    @Effect() AccountChangingPassword$ = this.actions$
    .pipe(
        ofType<AccountChangingPassword>(ACCOUNT_CHANGING_PASSWORD),
        map(action => action.payload),
        switchMap(changePasswordModel => this.authenticationService.ChangePassword(changePasswordModel)
            .pipe(
                map(data => {
                    return new AccountChangedPassword();
                }),
                catchError(err => {
                    let changePasswordModel = new ChangePasswordModel();
                    changePasswordModel.message = err;
                    return of(new AccountChangePasswordFailed(changePasswordModel));
                })
            )
        )
    );
}