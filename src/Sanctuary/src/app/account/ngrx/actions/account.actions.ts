import { Action } from "@ngrx/store";
import { LoginModel } from "account/models/login.model";
import { SignUpModel } from "account/models/signup.model";
import { RequestNewPasswordModel } from "account/models/requestnewpassword.model";
import { ChangePasswordModel } from "account/models/changepassword.model";

export const ACCOUNT_SIGNING_UP = 'ACCOUNT_SIGNING_UP';
export const ACCOUNT_CREATING = 'ACCOUNT_CREATING';
export const ACCOUNT_SIGN_UP_FAILED = 'ACCOUNT_SIGN_UP_FAILED';
export const ACCOUNT_CREATED = 'ACCOUNT_CREATED';
export const ACCOUNT_LOGGING_IN = 'ACCOUNT_LOGGING_IN';
export const ACCOUNT_LOGIN_FAILED = 'ACCOUNT_LOGIN_FAILED';
export const ACCOUNT_LOGGED_IN = 'ACCOUNT_LOGGED_IN';
export const ACCOUNT_LOGGING_OUT = 'ACCOUNT_LOGGING_OUT';
export const ACCOUNT_LOGGED_OUT = 'ACCOUNT_LOGGED_OUT';
export const ACCOUNT_REQUESTING_NEW_PASSWORD = 'ACCOUNT_REQUESTING_NEW_PASSWORD';
export const ACCOUNT_REQUESTED_NEW_PASSWORD = 'ACCOUNT_REQUESTED_NEW_PASSWORD';
export const ACCOUNT_CHANGING_PASSWORD = 'ACCOUNT_CHANGING_PASSWORD';
export const ACCOUNT_CHANGED_PASSWORD = 'ACCOUNT_CHANGED_PASSWORD';
export const ACCOUNT_CHANGE_PASSWORD_FAILED = 'ACCOUNT_CHANGE_PASSWORD_FAILED';

export class AccountSigningUp implements Action {
    type = ACCOUNT_SIGNING_UP;
    constructor(public payload: SignUpModel) { }
}

export class AccountCreating implements Action {
    type = ACCOUNT_CREATING;
    constructor(public payload: SignUpModel) { }
}

export class AccountSignUpFailed implements Action {
    type = ACCOUNT_SIGN_UP_FAILED;
    constructor(public payload: SignUpModel) { }
}

export class AccountCreated implements Action {
    type = ACCOUNT_CREATED
}

export class AccountLoggingIn implements Action {
    type = ACCOUNT_LOGGING_IN;
    constructor(public payload: LoginModel) { }
}

export class AccountLoginFailed implements Action {
    type = ACCOUNT_LOGIN_FAILED;
    constructor(public payload: LoginModel) { }
}

export class AccountLoggedIn implements Action {
    type = ACCOUNT_LOGGED_IN;
}

export class AccountLoggingOut implements Action {
    type = ACCOUNT_LOGGING_OUT;
}

export class AccountLoggedOut implements Action {
    type = ACCOUNT_LOGGED_OUT;
}

export class AccountRequestingNewPassword implements Action {
    type = ACCOUNT_REQUESTING_NEW_PASSWORD;
    constructor(public payload: RequestNewPasswordModel) { }
}

export class AccountRequestedNewPassword implements Action {
    type = ACCOUNT_REQUESTED_NEW_PASSWORD;
}

export class AccountChangingPassword implements Action {
    type = ACCOUNT_CHANGING_PASSWORD;
    constructor(public payload: ChangePasswordModel) { }
}

export class AccountChangedPassword implements Action {
    type = ACCOUNT_CHANGED_PASSWORD;
}

export class AccountChangePasswordFailed implements Action {
    type = ACCOUNT_CHANGE_PASSWORD_FAILED
    constructor(public payload: ChangePasswordModel) { }
}

export type AccountActions =    AccountSigningUp
                            |   AccountCreating
                            |   AccountSignUpFailed
                            |   AccountCreated
                            |   AccountLoggingIn
                            |   AccountLoginFailed
                            |   AccountLoggedIn
                            |   AccountLoggingOut
                            |   AccountLoggedOut
                            |   AccountRequestingNewPassword
                            |   AccountRequestedNewPassword
                            |   AccountChangingPassword
                            |   AccountChangePasswordFailed
                            |   AccountChangePasswordFailed;