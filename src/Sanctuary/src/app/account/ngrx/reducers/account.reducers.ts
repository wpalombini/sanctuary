import * as AccountActions from 'account/ngrx/actions/account.actions';
import { AccountState } from 'account/ngrx/reducers';



const initialState: AccountState = {
    accountSigningUp: null,
    accountSignUpFailed: null,
    accountCreated: false,
    accountLoggingIn: null,
    accountLoginFailed: null,
    accountLoggedIn: false,
    accountLoggingOut: false,
    accountLoggedOut: false,
    accountRequestingNewPassword: null,
    accountRequestedNewPassword: false,
    accountChangingPassword: null,
    accountChangedPassword: false,
    accountChangePasswordFailed: null,
}

export class AccountReducer {
    static accountReducer(state = initialState, action: AccountActions.AccountActions): AccountState {
        switch (action.type) {
            case AccountActions.ACCOUNT_SIGNING_UP: {
                state = Object.assign({}, {...initialState});
                state.accountSigningUp = (action as AccountActions.AccountSigningUp).payload;
                return state;
            }
            case AccountActions.ACCOUNT_CREATING: {
                state = Object.assign({}, {...initialState});
                state.accountSigningUp = (action as AccountActions.AccountCreating).payload;
                return state;
            }
            case AccountActions.ACCOUNT_SIGN_UP_FAILED: {
                state = Object.assign({}, {...initialState});
                state.accountSignUpFailed = (action as AccountActions.AccountSignUpFailed).payload;
                return state;
            }
            case AccountActions.ACCOUNT_CREATED: {
                state = Object.assign({}, initialState, { accountCreated: true})
                return state;
            }
            case AccountActions.ACCOUNT_LOGGING_IN: {
                state = Object.assign({}, {...initialState});
                state.accountLoggingIn = (action as AccountActions.AccountLoggingIn).payload;
                return state;
            }
            case AccountActions.ACCOUNT_LOGIN_FAILED: {
                state = Object.assign({}, {...initialState});
                state.accountLoginFailed = (action as AccountActions.AccountLoginFailed).payload;
                return state;
            }
            case AccountActions.ACCOUNT_LOGGED_IN: {
                state = Object.assign({}, initialState, { accountLoggedIn: true})
                return state;
            }
            case AccountActions.ACCOUNT_LOGGING_OUT: {
                state = Object.assign({}, initialState, { accountLoggingOut: true})
                return state; 
            }
            case AccountActions.ACCOUNT_LOGGED_OUT: {
                state = Object.assign({}, initialState, { accountLoggedOut: true})
                return state;
            }
            case AccountActions.ACCOUNT_REQUESTING_NEW_PASSWORD: {
                state = Object.assign({}, {...initialState});
                state.accountRequestingNewPassword = (action as AccountActions.AccountRequestingNewPassword).payload;
                return state;
            }
            case AccountActions.ACCOUNT_REQUESTED_NEW_PASSWORD: {
                state = Object.assign({}, initialState, { accountRequestedNewPassword: true})
                return state;
            }
            case AccountActions.ACCOUNT_CHANGING_PASSWORD: {
                state = Object.assign({}, {...initialState});
                state.accountChangingPassword = (action as AccountActions.AccountChangingPassword).payload;
                return state;
            }
            case AccountActions.ACCOUNT_CHANGED_PASSWORD: {
                state = Object.assign({}, {...initialState}, {accountChangedPassword: true});
                return state;
            }
            case AccountActions.ACCOUNT_CHANGE_PASSWORD_FAILED: {
                state = Object.assign({}, {...initialState});
                state.accountChangePasswordFailed = (action as AccountActions.AccountChangePasswordFailed).payload;
                return state;
            }
            default: {
                return state;
            }
        }
    }
}
