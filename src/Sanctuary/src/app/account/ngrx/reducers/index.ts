import { SignUpModel } from "account/models/signup.model";
import { LoginModel } from "account/models/login.model";
import { RequestNewPasswordModel } from "account/models/requestnewpassword.model";
import { ChangePasswordModel } from "account/models/changepassword.model";


export interface AccountState {
    accountSigningUp: SignUpModel;
    accountSignUpFailed: SignUpModel;
    accountCreated: boolean;
    accountLoggingIn: LoginModel;
    accountLoginFailed: LoginModel;
    accountLoggedIn: boolean;
    accountLoggingOut: boolean;
    accountLoggedOut: boolean;
    accountRequestingNewPassword: RequestNewPasswordModel;
    accountRequestedNewPassword: boolean;
    accountChangingPassword: ChangePasswordModel;
    accountChangedPassword: boolean;
    accountChangePasswordFailed: ChangePasswordModel;
}