﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/components/home.component';
import { ContactComponent } from './contact/components/contact.component';
import { AuthenticationGuard } from 'shared/guard/authentication.guard';


const appRoutes: Routes = [
    {
        path: 'account',
        loadChildren: './account/account.module#AccountModule'
    },
    {
        path: 'contact',
        component: ContactComponent
    },
    {
        path: 'sanctuary',
        loadChildren: './sanctuary/sanctuary.module#SanctuaryModule',
        canLoad: [AuthenticationGuard]
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: '',
        component: HomeComponent
    },
    {
        path: '**',
        component: HomeComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    providers: [
        AuthenticationGuard
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }