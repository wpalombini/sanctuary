//css
import 'font-awesome/scss/font-awesome.scss';
import 'bootstrap/dist/css/bootstrap.css';

import './assets/css/loader.scss';
import './assets/css/bs-override.scss';
import './assets/css/styles.scss';


//js
import 'jquery';
import 'bootstrap/dist/js/bootstrap';
import 'auth0-js';

import './assets/js/base.js';