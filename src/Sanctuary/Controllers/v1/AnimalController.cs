﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sanctuary.Domain.Interfaces.Services;
using Sanctuary.Domain.Dtos;
using Microsoft.AspNetCore.Authorization;
using Sanctuary.Domain.Helpers;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sanctuary.Controllers.v1
{
    [Authorize(Policy = Global.CustomAuthorization)]
    [Route("api/v1/[controller]")]
    public class AnimalController : BaseController
    {
        private readonly IAnimalService _animalService;

        public AnimalController(IAnimalService animalService)
        {
            this._animalService = animalService;
        }

        [AllowAnonymous]
        [HttpGet, Route("GetSpeciesAndBreeds")]
        public async Task<AnimalSpeciesAndBreedDto> GetSpeciesAndBreeds(SanctuaryDto sanctuaryDto)
        {
            return await this._animalService.GetSpeciesAndBreeds(sanctuaryDto);
        }

        [HttpPost, Route("Create")]
        public async Task<AnimalDto> Create([FromBody] AnimalDto animalDto)
        {
            return await this._animalService.Create(animalDto);
        }

        [HttpGet, Route("GetAnimals")]
        public async Task<IEnumerable<AnimalDto>> GetAnimals(AnimalsFilterDto filter)
        {
            filter.AccountId = Guid.Parse(User.Identity.Name);
            return await this._animalService.GetAnimals(filter);
        }

        [HttpGet, Route("GetAnimalById")]
        public async Task<AnimalDto> GetAnimalById(string id)
        {
            return await this._animalService.GetAnimalById(id);
        }
    }
}
