﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sanctuary.Domain.Interfaces.Services;
using Sanctuary.Domain.Dtos;
using Microsoft.AspNetCore.Authorization;
using Sanctuary.Domain.Helpers;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sanctuary.Controllers.v1
{
    [Authorize(Policy = Global.CustomAuthorization)]
    [Route("api/v1/[controller]")]
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            this._accountService = accountService;
        }

        [HttpGet, Route("")]
        public async Task<AccountDto> GetAccountDetails()
        {
            return await this._accountService
                .GetAccountDetails(new AccountDto() { Id = Guid.Parse(User.Identity.Name) });
        }

        [AllowAnonymous]
        [HttpPost, Route("Create")]
        public async Task<AccountDto> Create([FromBody] AccountDto newAccountDto)
        {
            return await this._accountService.CreateAccount(newAccountDto);
        }

        [HttpPost, Route("EditContactDetails")]
        public async Task<AccountDto> EditContactDetails([FromBody] AccountDto accountDto)
        {
            accountDto.Id = Guid.Parse(User.Identity.Name);
            return await this._accountService.EditContactDetails(accountDto);
        }
    }
}
