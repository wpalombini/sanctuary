﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Helpers;

namespace Sanctuary.Controllers.v1
{
    [Authorize(Policy = Global.CustomAuthorization)]
    [Route("api/v1/[controller]")]
    public class AuthenticationController : Controller
    {
        private readonly Domain.Interfaces.Services.IAuthenticationService _authenticationService;

        public AuthenticationController(Domain.Interfaces.Services.IAuthenticationService authenticationService)
        {
            this._authenticationService = authenticationService;
        }

        [HttpPost, Route("ChangePassword")]
        public async Task<bool> ChangePassword([FromBody]ChangePasswordDto changePasswordDto)
        {
            changePasswordDto.AccountId = Guid.Parse(User.Identity.Name);
            return await this._authenticationService.ChangePassword(changePasswordDto);
        }
    }
}