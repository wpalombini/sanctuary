﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sanctuary.Domain.Interfaces.Services;
using Sanctuary.Domain.Dtos;
using Microsoft.AspNetCore.Hosting;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sanctuary.Controllers.v1
{
    [Route("api/v1/[controller]")]
    public class ContactController : BaseController
    {
        private readonly IContactService _contactService;
        private readonly IHostingEnvironment _hostEnvironment;

        public ContactController(IContactService contactService, IHostingEnvironment hostingEnvironment)
        {
            this._contactService = contactService;
            this._hostEnvironment = hostingEnvironment;
        }

        // POST api/values
        [HttpPost, Route("PostMessage", Name = "postmessage")]
        public async Task<ContactDto> Post([FromBody]ContactDto message)
        {
            //throw new Exception("testing error");

            _contactService.SetRootPath(this._hostEnvironment.WebRootPath);

            await _contactService.Send(message);

            return message;
        }
    }
}
