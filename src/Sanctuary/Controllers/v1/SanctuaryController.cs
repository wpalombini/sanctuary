﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sanctuary.Domain.Interfaces.Services;
using Sanctuary.Domain.Dtos;
using Microsoft.AspNetCore.Authorization;
using Sanctuary.Domain.Helpers;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sanctuary.Controllers.v1
{
    [Authorize(Policy = Global.CustomAuthorization)]
    [Route("api/v1/[controller]")]
    public class SanctuaryController : BaseController
    {
        private readonly ISanctuaryService _sanctuaryService;

        public SanctuaryController(ISanctuaryService sanctuaryService)
        {
            this._sanctuaryService = sanctuaryService;
        }

        [HttpPost, Route("Create")]
        public async Task<SanctuaryDto> Create([FromBody] SanctuaryDto newSanctuaryDto)
        {
            newSanctuaryDto.AccountId = Guid.Parse(User.Identity.Name);
            return await this._sanctuaryService.CreateSanctuary(newSanctuaryDto);
        }

        [HttpPost, Route("Edit")]
        public async Task<SanctuaryDto> Edit([FromBody] SanctuaryDto sanctuaryDto)
        {
            sanctuaryDto.AccountId = Guid.Parse(User.Identity.Name);
            return await this._sanctuaryService.EditSanctuary(sanctuaryDto);
        }

        [HttpGet, Route("GetSanctuariesForAccountId")]
        public async Task<IEnumerable<SanctuaryDto>> GetSanctuariesForAccountId()
        {
            return await this._sanctuaryService.GetSanctuariesForAccountId(Guid.Parse(User.Identity.Name));
        }
    }
}
