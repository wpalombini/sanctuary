﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sanctuary.Domain.Interfaces.Services;
using Sanctuary.Domain.Dtos;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Sanctuary.Controllers.v1
{
    [Route("api/v1/[controller]")]
    public class HomeController : BaseController
    {
        private readonly IAccountService _accountService;

        public HomeController(IAccountService accountService)
        {
            this._accountService = accountService;
        }

        // GET: api/values
        [HttpGet, Route("GetSkills", Name = "getskills")]
        public async Task<IEnumerable<AccountDto>> GetSkills()
        {
            var test = await this._accountService.GetAll();

            return await this._accountService.GetAll();
        }
    }
}
