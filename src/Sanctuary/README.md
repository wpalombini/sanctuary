﻿//run npm install in order to get all the packages
npm install

//run npm run build:prod in order to get the creds.json copied to the wwwroot folder
npm run build:prod

//run npm start to start the webpack dev server
npm start

//run iisexpress to fire up the web api
in visual studio, run IIS Express

//launch the browser and navigate to localhost:8080