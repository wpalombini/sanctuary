﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using Sanctuary.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Sanctuary.Components.Filters
{
    public class CustomAuthorizationRequirement : IAuthorizationRequirement { }

    public class CustomAuthorizationHandler : AuthorizationHandler<CustomAuthorizationRequirement>
    {
        private IAccountService _accountService;

        public CustomAuthorizationHandler(IAccountService accountService)
        {
            this._accountService = accountService;
        }

        protected async override Task HandleRequirementAsync(AuthorizationHandlerContext context, CustomAuthorizationRequirement requirement)
        {
            var httpContext = (context.Resource as AuthorizationFilterContext).HttpContext;

            if (httpContext.User.Identity.IsAuthenticated)
            {
                string authenticationDetails = extractAuthenticationDetails(httpContext.User);

                if (!string.IsNullOrEmpty(authenticationDetails))
                {
                    var accountId = await this._accountService.GetAccountIdByAuthentication(authenticationDetails);

                    if (accountId != Guid.Empty)
                    {
                        //All good
                        context.Succeed(requirement);

                        var claims = httpContext.User.Claims.ToList();

                        claims.Add(new Claim(ClaimTypes.Name, accountId.ToString()));

                        var claimsIdentity = new ClaimsIdentity(claims, "Bearer");

                        httpContext.User = new ClaimsPrincipal(claimsIdentity);

                        return;
                    }
                }
            }

            context.Fail();
        }

        private string extractAuthenticationDetails(ClaimsPrincipal identity)
        {
            return identity.Claims
                            .Where(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")
                            .Select(s => s.Value)
                            .FirstOrDefault();
        }
    }
}
