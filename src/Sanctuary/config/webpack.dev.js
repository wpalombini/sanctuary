var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

module.exports = webpackMerge(commonConfig, {
    //devtool: 'source-map', //to simulate production with bundled and minified scripts
    devtool: 'cheap-module-eval-source-map', //for debugging on the browser (dev tools)

    entry: {
        'polyfills': './src/polyfills.ts',
        'vendor': './src/vendor.ts',
        'app': './src/main.ts'
    },
    
    output: {
        path: helpers.root('wwwroot'),
        publicPath: '/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    plugins: [
      new ExtractTextPlugin('[name].css')
    ],

    devServer: {
        inline: false,
        historyApiFallback: true,
        //logLevel: 'debug',
        stats: 'minimal',
        https: true,
        port: 8080,
        disableHostCheck: true,
        compress: true
    }
});
