var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var helpers = require('./helpers');

module.exports = {
    
    resolve: {
        extensions: ['.ts', '.js'],
        modules: [
            //needed for non-relative paths in tsconfig.json
            "../src",
            "../node_modules",
            "../src/app"
        ]
    },

    optimization: {
        splitChunks: {
            chunks: "all"
          }
    },

    module: {
        rules: [
          {
              test: /\.ts$/,
              loaders: [
                {
                    loader: 'awesome-typescript-loader',
                    options: { configFileName: helpers.root('src', 'tsconfig.json') }
                }, 'angular2-template-loader', 'angular-router-loader'
              ]
          },
          {
              test: /\.html$/,
              use: 'html-loader'
          },
          {
              test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
              include: helpers.root('src', 'app'),
              use: 'file-loader?name=assets/[name].[hash].[ext]'
          },
          {
              test: /\.css$/,
              exclude: helpers.root('src', 'app'),
              use: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader?minimize' })
          },
          {
              test: /\.css$/,
              include: helpers.root('src', 'app'),
              use: 'raw-loader'
          },
          {
              test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
              use: 'file-loader?name=assets/[name].[hash].[ext]',
          },
          {
              test: /\.scss$/,
              loaders: ["style-loader", "css-loader", "sass-loader"],
          }
        ]
    },

    

    plugins: [
      // Workaround for angular/angular#11580
      new webpack.ContextReplacementPlugin(
        // The (\\|\/) piece accounts for path separators in *nix and Windows
        /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
        helpers.root('./src'), // location of your src
        {} // a map of your routes
      ),

      new webpack.ProvidePlugin({
          $: "jquery",
          jQuery: "jquery",
          "window.jQuery": "jquery"
      }),

      new webpack.ProvidePlugin({
          Reflect: 'core-js/es7/reflect'
      }),

      new CopyWebpackPlugin([
          //{
          //    from: helpers.root('src', 'assets', 'css'),
          //    to: 'assets/css'
          //},
          //{
          //    from: helpers.root('src', 'assets', 'fonts'),
          //    to: 'assets/fonts'
          //},
          //{
          //    from: helpers.root('src', 'assets', 'js'),
          //    to: 'assets/js'
          //},
          {
              from: helpers.root('content', 'templates'),
              to: 'content/templates'
          },
          {
              from: helpers.root('src', 'assets', 'img'),
              to: 'assets/img'
          },
          {
              from: helpers.root('./creds.json'),
              to: 'creds.json'
          }
      ],
          { copyUnmodified: true }
      ),

    //   new webpack.optimize.CommonsChunkPlugin({
    //       name: ['app', 'vendor', 'polyfills'],
    //       minChunks: Infinity
    //   }),

      new HtmlWebpackPlugin({
          template: 'src/index.html',
          chunksSortMode: 'none'
      })
    ]
};

