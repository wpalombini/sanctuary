﻿using Microsoft.EntityFrameworkCore;
using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Interfaces.Repositories;
using Sanctuary.Repositories.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanctuary.Repositories.Repos
{
    public class AnimalRepository : BaseRepository<Animal>, IAnimalRepository
    {
        public AnimalRepository(DataContext dataContext) : base(dataContext)
        {

        }

        public async Task<IEnumerable<AnimalBreed>> GetAnimalBreeds()
        {
            return await _dataContext
                .Set<AnimalBreed>()
                .Where(x => x.SanctuaryId.HasValue == false)
                .ToListAsync();
        }

        public async Task<IEnumerable<AnimalBreed>> GetAnimalBreeds(Guid sanctuaryId)
        {
            return await _dataContext
                .Set<AnimalBreed>()
                .Where(x => x.SanctuaryId.HasValue == false || x.SanctuaryId == sanctuaryId)
                .ToListAsync();
        }

        public async Task<IEnumerable<AnimalSpecies>> GetAnimalSpecies()
        {
            return await _dataContext
                .Set<AnimalSpecies>()
                .Where(x => x.SanctuaryId.HasValue == false)
                .ToListAsync();
        }

        public async Task<IEnumerable<AnimalSpecies>> GetAnimalSpecies(Guid sanctuaryId)
        {
            return await _dataContext
                .Set<AnimalSpecies>()
                .Where(x => x.SanctuaryId.HasValue == false || x.SanctuaryId == sanctuaryId)
                .ToListAsync();
        }

        public async Task<IEnumerable<Animal>> GetAnimalsByFilter(Domain.Dtos.AnimalsFilterDto filter)
        {
            var query = (from an in base._dataContext.Animal
                         join sa in base._dataContext.Sanctuary on an.SanctuaryId equals sa.Id
                         join accsan in base._dataContext.AccountSanctuary on sa.Id equals accsan.SanctuaryId
                         join ac in base._dataContext.Account on accsan.AccountId equals ac.Id
                         where ac.Id == filter.AccountId
                         && ac.DeletedAt.HasValue == false
                         && sa.Id == filter.SanctuaryId
                         select an
                         );
                         

            if (filter.AnimalSpeciesId != null && filter.AnimalSpeciesId != Guid.Empty)
            {
                query = (from an in query
                         join anbreed in base._dataContext.AnimalBreed on an.AnimalBreedId equals anbreed.Id
                         join anspe in base._dataContext.AnimalSpecies on anbreed.AnimalSpeciesId equals anspe.Id
                         where anspe.Id == filter.AnimalSpeciesId
                         select an);
            }

            if (filter.AnimalBreedId != null && filter.AnimalBreedId != Guid.Empty)
            {
                query = (from an in query
                         join anbreed in base._dataContext.AnimalBreed on an.AnimalBreedId equals anbreed.Id
                         where anbreed.Id == filter.AnimalBreedId
                         select an);
            }

            if (!string.IsNullOrEmpty(filter.AnimalName))
            {
                query = query.Where(x => x.Name == filter.AnimalName);
            }
            
            return await query
                .Include(i => i.AnimalBreed)
                .Include(i => i.AnimalBreed.AnimalSpecies)
                .OrderByDescending(o => o.CreatedAt)
                .Skip(filter.ResultsPerPage * (filter.Page - 1))
                .Take(filter.ResultsPerPage)
                .ToListAsync();
        }

        public async Task<Animal> GetAnimalById(Guid id)
        {
            return await _dataContext
                .Set<Animal>()
                .Include(i => i.AnimalBreed)
                .Include(i => i.AnimalBreed.AnimalSpecies)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
        }
    }
}
