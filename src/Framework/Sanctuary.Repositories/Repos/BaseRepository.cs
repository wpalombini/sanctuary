﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sanctuary.Domain.Interfaces.Repositories;
using Sanctuary.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Sanctuary.Repositories.Context;

namespace Sanctuary.Repositories.Repos
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : BaseDatabaseEntity
    {
        protected DataContext _dataContext;

        public BaseRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public virtual async Task AddAsync(T obj)
        {
            await _dataContext.Set<T>().AddAsync(obj);
        }

        public virtual async Task<bool> DeleteAsync(Guid Id)
        {
            T obj = await GetByIdAsync(Id);

            bool found = (obj != null);
            if (found)
            {
                _dataContext.Remove(obj);
            }
            return found;
        }

        public virtual async Task<IEnumerable<T>> GetAllWithNoTrackingAsync()
        {
            return await _dataContext
                .Set<T>()
                .AsNoTracking()
                .ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(Guid Id)
        {
            return await _dataContext
                .Set<T>()
                .FindAsync(Id);
        }

        public virtual async Task UpdateAsync(T obj)
        {
            await Task.Run(() =>
            {
                _dataContext
                    .Set<T>()
                    .Update(obj);
            });
        }

        public virtual Task<int> SaveAsync()
        {
            return _dataContext.SaveChangesAsync();
        }
    }
}
