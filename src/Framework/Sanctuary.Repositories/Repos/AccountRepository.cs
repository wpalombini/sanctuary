﻿using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Interfaces.Repositories;
using Sanctuary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sanctuary.Repositories.Context;
using Microsoft.EntityFrameworkCore;

namespace Sanctuary.Repositories.Repos
{
    public class AccountRepository : BaseRepository<Account>, IAccountRepository
    {
        public AccountRepository(DataContext dataContext) : base(dataContext)
        {
            
        }

        public async Task<Account> GetActiveAccountByEmailAsync(string email)
        {
            Account account = null;

            account = await base._dataContext.Set<Account>()
                .FirstOrDefaultAsync(f => f.EmailAddress == email && f.DeletedAt.HasValue == false);

            return account;
        }

        public async Task<Account> GetAccountByAuthenticationDetails(string provider, string authenticationId)
        {
            Account account = null;

            account = account = await base._dataContext.Set<Account>()
                .FirstOrDefaultAsync(f => f.Provider == provider && f.AuthenticationId == authenticationId);

            return account;
        }
    }
}
