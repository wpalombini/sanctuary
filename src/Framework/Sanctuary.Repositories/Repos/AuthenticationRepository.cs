﻿using Newtonsoft.Json;
using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Sanctuary.Repositories.Repos
{
    public class AuthenticationRepository : IAuthenticationRepository
    {
        private readonly IAccountRepository _accountRepository;

        public AuthenticationRepository(IAccountRepository accountRepository)
        {
            this._accountRepository = accountRepository;
        }

        public async Task<bool> ChangePassword(ChangePasswordDto changePasswordDto)
        {
            using (HttpClient client = new HttpClient())
            {
                var managementTokenModel = await this.GetManagementTokenModel();

                string authenticationId = await this.GetAuthenticationIdByAccountId(changePasswordDto.AccountId);
                string url = $"https://wpalombini.au.auth0.com/api/v2/users/{authenticationId}";

                var content = new
                {
                    password = changePasswordDto.Password,
                    client_id = "zfmmXYYU2Oyy0F1tbWLYqmihpYit7y16",
                    connection = "Username-Password-Authentication-Development"
                };

                HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(content));
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var patchRequest = new HttpRequestMessage(new HttpMethod("PATCH"), url)
                {
                    Content = httpContent
                };
                patchRequest.Headers.Add("Authorization", $"{managementTokenModel.TokenType} {managementTokenModel.AccessToken}");



                var response = await client.SendAsync(patchRequest);

                var test = await response.Content.ReadAsStringAsync();

                response.EnsureSuccessStatusCode();

                var result = await response.Content.ReadAsStringAsync();

                return true;

            }
        }

        private async Task<ManagementTokenModel> GetManagementTokenModel()
        {
            using (HttpClient client = new HttpClient())
            {
                var managementContent = new
                {
                    client_id = "KolPaGOXHAXlYF2Fm1wgvqja1RYcD7j1",
                    client_secret = "EwenVYtGjGY9rqLnjQFVTVA4g0E_Dw_l8X_SMcacjodngVlSWXf-LDiy1RjlZm8t",
                    audience = "https://wpalombini.au.auth0.com/api/v2/",
                    grant_type = "client_credentials"
                };
                HttpContent strContent = new StringContent(JsonConvert.SerializeObject(managementContent));
                strContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var response = await client.PostAsync("https://wpalombini.au.auth0.com/oauth/token", strContent);

                response.EnsureSuccessStatusCode();

                string result = await response.Content.ReadAsStringAsync();

                var managementTokenModel = JsonConvert.DeserializeObject<ManagementTokenModel>(result);

                return managementTokenModel;
            }
        }

        private async Task<string> GetAuthenticationIdByAccountId(Guid accountId)
        {
            var account = await this._accountRepository.GetByIdAsync(accountId);

            return $"{account.Provider}|{account.AuthenticationId}";
        }

        class ManagementTokenModel
        {
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }

            [JsonProperty("expires_in")]
            public int ExpiresIn { get; set; }

            [JsonProperty("token_type")]
            public string TokenType { get; set; }
        }
        
    }
}
