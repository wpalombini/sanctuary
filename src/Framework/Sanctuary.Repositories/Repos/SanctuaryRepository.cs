﻿using Sanctuary.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sanctuary.Repositories.Context;
using Microsoft.EntityFrameworkCore;
using Sanctuary.Domain.Entities;

namespace Sanctuary.Repositories.Repos
{
    public class SanctuaryRepository : BaseRepository<Domain.Entities.Sanctuary>, ISanctuaryRepository
    {
        public SanctuaryRepository(DataContext dataContext) : base(dataContext)
        {

        }

        public async Task<Domain.Entities.Sanctuary> GetBySanctuaryName(string name)
        {
            if (string.IsNullOrEmpty(name))
                return null;

            return await base._dataContext
                .Set<Domain.Entities.Sanctuary>()
                .FirstOrDefaultAsync(f => f.Name == name);
        }

        public async Task<IEnumerable<Domain.Entities.Sanctuary>> GetSanctuariesForAccountId(Guid accountId)
        {
            return await (from s in base._dataContext.Set<Domain.Entities.Sanctuary>()
                          join accsan in base._dataContext.Set<AccountSanctuary>() on s.Id equals accsan.SanctuaryId
                          where accsan.AccountId == accountId
                          select s)
                          .ToListAsync();
        }

        public async Task<Domain.Entities.Sanctuary> GetSanctuaryByIdForAccountId(Guid sanctuaryId, Guid accountId)
        {
            return await (from s in base._dataContext.Set<Domain.Entities.Sanctuary>()
                          join accsan in base._dataContext.Set<AccountSanctuary>() on s.Id equals accsan.SanctuaryId
                          where accsan.AccountId == accountId && accsan.SanctuaryId == sanctuaryId
                          select s)
                          .FirstOrDefaultAsync();
        }
    }
}
