﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Sanctuary.Repositories.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Provider = table.Column<string>(type: "varchar(50)", nullable: false),
                    AuthenticationId = table.Column<string>(type: "varchar(100)", nullable: false),
                    EmailAddress = table.Column<string>(type: "varchar(100)", nullable: false),
                    FirstName = table.Column<string>(type: "varchar(100)", nullable: true),
                    IsAdmin = table.Column<bool>(nullable: false),
                    LastName = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sanctuary",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    FullAddress = table.Column<string>(type: "varchar(1000)", nullable: false),
                    Name = table.Column<string>(type: "varchar(200)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sanctuary", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AccountSanctuary",
                columns: table => new
                {
                    AccountId = table.Column<Guid>(nullable: false),
                    SanctuaryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountSanctuary", x => new { x.AccountId, x.SanctuaryId });
                    table.ForeignKey(
                        name: "FK_AccountSanctuary_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountSanctuary_Sanctuary_SanctuaryId",
                        column: x => x.SanctuaryId,
                        principalTable: "Sanctuary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AnimalSpecies",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(type: "varchar(50)", nullable: false),
                    SanctuaryId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnimalSpecies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnimalSpecies_Sanctuary_SanctuaryId",
                        column: x => x.SanctuaryId,
                        principalTable: "Sanctuary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AnimalBreed",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AnimalSpeciesId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", nullable: false),
                    SanctuaryId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnimalBreed", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnimalBreed_AnimalSpecies_AnimalSpeciesId",
                        column: x => x.AnimalSpeciesId,
                        principalTable: "AnimalSpecies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AnimalBreed_Sanctuary_SanctuaryId",
                        column: x => x.SanctuaryId,
                        principalTable: "Sanctuary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Animal",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AnimalBreedId = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Gender = table.Column<string>(type: "varchar(1)", nullable: false),
                    Name = table.Column<string>(type: "varchar(50)", nullable: false),
                    SanctuaryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Animal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Animal_AnimalBreed_AnimalBreedId",
                        column: x => x.AnimalBreedId,
                        principalTable: "AnimalBreed",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Animal_Sanctuary_SanctuaryId",
                        column: x => x.SanctuaryId,
                        principalTable: "Sanctuary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountSanctuary_SanctuaryId",
                table: "AccountSanctuary",
                column: "SanctuaryId");

            migrationBuilder.CreateIndex(
                name: "IX_Animal_AnimalBreedId",
                table: "Animal",
                column: "AnimalBreedId");

            migrationBuilder.CreateIndex(
                name: "IX_Animal_SanctuaryId",
                table: "Animal",
                column: "SanctuaryId");

            migrationBuilder.CreateIndex(
                name: "IX_AnimalBreed_AnimalSpeciesId",
                table: "AnimalBreed",
                column: "AnimalSpeciesId");

            migrationBuilder.CreateIndex(
                name: "IX_AnimalBreed_SanctuaryId",
                table: "AnimalBreed",
                column: "SanctuaryId");

            migrationBuilder.CreateIndex(
                name: "IX_AnimalSpecies_SanctuaryId",
                table: "AnimalSpecies",
                column: "SanctuaryId");

            //Seeding data
            string sql = @"
INSERT INTO dbo.AnimalSpecies (Id, [Name], SanctuaryId)
VALUES
(NEWID(), 'Dog', null),
(NEWID(), 'Cat', null),
(NEWID(), 'Sheep', null),
(NEWID(), 'Rabbit', null),
(NEWID(), 'Chicken', null),
(NEWID(), 'Pig', null),
(NEWID(), 'Horse', null),
(NEWID(), 'Cattle', null),
(NEWID(), 'Duck', null)


INSERT INTO dbo.AnimalBreed (Id, AnimalSpeciesId, [Name], SanctuaryId)
VALUES
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Dog'), 'Golden Retriever', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Dog'), 'Kelpie', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Dog'), 'Staffordshire Bull Terrier', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Dog'), 'Mix', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Dog'), 'Greyhound', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Dog'), 'Husky', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Dog'), 'Unknown', null),

(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cat'), 'Domestic Long Hair', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cat'), 'Domestic Short Hair', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cat'), 'Burmese', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cat'), 'Siamese', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cat'), 'Ragdoll', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cat'), 'Bengal', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cat'), 'Unknown', null),

(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Sheep'), 'Dorper', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Sheep'), 'Suffolk', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Sheep'), 'Merino', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Sheep'), 'Corriedale', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Sheep'), 'Unknown', null),

(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Rabbit'), 'Angora', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Rabbit'), 'Mini Lop', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Rabbit'), 'Holland Lop', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Rabbit'), 'Unknown', null),

(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Chicken'), 'Rhode Island Red', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Chicken'), 'Australorp', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Chicken'), 'Dominique', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Chicken'), 'Unknown', null),

(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Pig'), 'American Yorkshire', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Pig'), 'Tamworth', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Pig'), 'Hampshire', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Pig'), 'Unknown', null),

(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Horse'), 'Poney', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Horse'), 'Arabian', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Horse'), 'Mix', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Horse'), 'Unknown', null),

(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cattle'), 'Angus', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cattle'), 'Holstein Friesian', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cattle'), 'Shorthorn', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cattle'), 'Red Poll', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cattle'), 'Hereford', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cattle'), 'Mix', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Cattle'), 'Unknown', null),

(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Duck'), 'Rouen', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Duck'), 'American Pekin', null),
(NEWID(), (SELECT Id from AnimalSpecies WHERE Name = 'Duck'), 'Unknown', null)



                            ";

            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountSanctuary");

            migrationBuilder.DropTable(
                name: "Animal");

            migrationBuilder.DropTable(
                name: "AnimalBreed");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "AnimalSpecies");

            migrationBuilder.DropTable(
                name: "Sanctuary");
        }
    }
}
