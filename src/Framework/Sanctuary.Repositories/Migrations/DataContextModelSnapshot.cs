﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Sanctuary.Repositories.Context;

namespace Sanctuary.Repositories.Migrations
{
    [DbContext(typeof(DataContext))]
    partial class DataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Sanctuary.Domain.Entities.Account", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Provider")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("AuthenticationId")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("EmailAddress")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("FirstName")
                        .HasColumnType("varchar(100)");

                    b.Property<bool>("IsAdmin");

                    b.Property<string>("LastName")
                        .HasColumnType("varchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Account");
                });

            modelBuilder.Entity("Sanctuary.Domain.Entities.AccountSanctuary", b =>
                {
                    b.Property<Guid>("AccountId");

                    b.Property<Guid>("SanctuaryId");

                    b.HasKey("AccountId", "SanctuaryId");

                    b.HasIndex("SanctuaryId");

                    b.ToTable("AccountSanctuary");
                });

            modelBuilder.Entity("Sanctuary.Domain.Entities.Animal", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("AnimalBreedId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DateOfBirth");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Gender")
                        .IsRequired()
                        .HasColumnType("varchar(1)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<Guid>("SanctuaryId");

                    b.HasKey("Id");

                    b.HasIndex("AnimalBreedId");

                    b.HasIndex("SanctuaryId");

                    b.ToTable("Animal");
                });

            modelBuilder.Entity("Sanctuary.Domain.Entities.AnimalBreed", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("AnimalSpeciesId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<Guid?>("SanctuaryId");

                    b.HasKey("Id");

                    b.HasIndex("AnimalSpeciesId");

                    b.HasIndex("SanctuaryId");

                    b.ToTable("AnimalBreed");
                });

            modelBuilder.Entity("Sanctuary.Domain.Entities.AnimalSpecies", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<Guid?>("SanctuaryId");

                    b.HasKey("Id");

                    b.HasIndex("SanctuaryId");

                    b.ToTable("AnimalSpecies");
                });

            modelBuilder.Entity("Sanctuary.Domain.Entities.Sanctuary", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("FullAddress")
                        .IsRequired()
                        .HasColumnType("varchar(1000)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(200)");

                    b.HasKey("Id");

                    b.ToTable("Sanctuary");
                });

            modelBuilder.Entity("Sanctuary.Domain.Entities.AccountSanctuary", b =>
                {
                    b.HasOne("Sanctuary.Domain.Entities.Account", "Account")
                        .WithMany("AccountSanctuaries")
                        .HasForeignKey("AccountId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Sanctuary.Domain.Entities.Sanctuary", "Sanctuary")
                        .WithMany("AccountSanctuaries")
                        .HasForeignKey("SanctuaryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Sanctuary.Domain.Entities.Animal", b =>
                {
                    b.HasOne("Sanctuary.Domain.Entities.AnimalBreed", "AnimalBreed")
                        .WithMany("Animals")
                        .HasForeignKey("AnimalBreedId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Sanctuary.Domain.Entities.Sanctuary", "Sanctuary")
                        .WithMany("Animals")
                        .HasForeignKey("SanctuaryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Sanctuary.Domain.Entities.AnimalBreed", b =>
                {
                    b.HasOne("Sanctuary.Domain.Entities.AnimalSpecies", "AnimalSpecies")
                        .WithMany("AnimalBreeds")
                        .HasForeignKey("AnimalSpeciesId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Sanctuary.Domain.Entities.Sanctuary", "Sanctuary")
                        .WithMany("AnimalBreeds")
                        .HasForeignKey("SanctuaryId");
                });

            modelBuilder.Entity("Sanctuary.Domain.Entities.AnimalSpecies", b =>
                {
                    b.HasOne("Sanctuary.Domain.Entities.Sanctuary", "Sanctuary")
                        .WithMany("AnimalSpecies")
                        .HasForeignKey("SanctuaryId");
                });
        }
    }
}
