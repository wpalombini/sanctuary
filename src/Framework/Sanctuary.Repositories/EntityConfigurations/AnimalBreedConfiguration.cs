﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Repositories.EntityConfigurations
{
    public class AnimalBreedConfiguration
    {
        public AnimalBreedConfiguration(EntityTypeBuilder<AnimalBreed> entity)
        {
            entity.ToTable("AnimalBreed");

            entity.HasKey(k => k.Id);

            entity.Property(p => p.Name)
                .IsRequired()
                .HasColumnType("varchar(100)");

            entity.HasOne(x => x.AnimalSpecies)
                .WithMany(x => x.AnimalBreeds)
                .IsRequired();

            entity.HasOne(x => x.Sanctuary)
                .WithMany(x => x.AnimalBreeds);
        }
    }
}
