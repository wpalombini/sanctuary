﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Repositories.EntityConfigurations
{
    public class AnimalSpeciesConfiguration
    {
        public AnimalSpeciesConfiguration(EntityTypeBuilder<AnimalSpecies> entity)
        {
            entity.ToTable("AnimalSpecies");

            entity.HasKey(k => k.Id);

            entity.Property(p => p.Name)
                .IsRequired()
                .HasColumnType("varchar(50)");

            entity.HasOne(x => x.Sanctuary)
                .WithMany(x => x.AnimalSpecies);
        }
    }
}
