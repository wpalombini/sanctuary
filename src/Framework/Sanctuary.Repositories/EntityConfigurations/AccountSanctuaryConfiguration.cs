﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Repositories.EntityConfigurations
{
    public class AccountSanctuaryConfiguration
    {
        public AccountSanctuaryConfiguration(EntityTypeBuilder<AccountSanctuary> entity)
        {
            entity.ToTable("AccountSanctuary");

            entity.Ignore(i => i.Id);

            entity.HasKey(k => new { k.AccountId, k.SanctuaryId });

            entity.HasOne(x => x.Account)
                .WithMany(x => x.AccountSanctuaries)
                .IsRequired();

            entity.HasOne(x => x.Sanctuary)
                .WithMany(x => x.AccountSanctuaries)
                .IsRequired();
        }
    }
}
