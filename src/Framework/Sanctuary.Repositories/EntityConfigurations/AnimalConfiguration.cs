﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Repositories.EntityConfigurations
{
    public class AnimalConfiguration
    {
        public AnimalConfiguration(EntityTypeBuilder<Animal> entity)
        {
            entity.ToTable("Animal");

            entity.HasKey(k => k.Id);

            entity.Property(p => p.Name)
                .IsRequired()
                .HasColumnType("varchar(50)");

            entity.Property(p => p.Gender)
                .IsRequired()
                .HasColumnType("varchar(1)");

            entity.Property(p => p.CreatedAt)
                .IsRequired();

            entity.HasOne(x => x.AnimalBreed)
                .WithMany(x => x.Animals)
                .IsRequired();

            entity.HasOne(x => x.Sanctuary)
                .WithMany(x => x.Animals)
                .IsRequired();
        }
    }
}
