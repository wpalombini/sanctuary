﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Repositories.EntityConfigurations
{
    public class AccountConfiguration
    {
        public AccountConfiguration(EntityTypeBuilder<Account> entity)
        {
            entity.ToTable("Account");

            entity.HasKey(k => k.Id);

            entity.Property(p => p.Provider)
                .IsRequired()
                .HasColumnType("varchar(50)");

            entity.Property(p => p.AuthenticationId)
                .IsRequired()
                .HasColumnType("varchar(100)");

            entity.Property(p => p.EmailAddress)
                .IsRequired()
                .HasColumnType("varchar(100)");

            entity.Property(p => p.FirstName)
                .HasColumnType("varchar(100)");

            entity.Property(p => p.LastName)
                .HasColumnType("varchar(100)");

            entity.Property(p => p.IsAdmin)
                .IsRequired();

            entity.Property(p => p.CreatedAt)
                .IsRequired();
        }
    }
}
