﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Repositories.EntityConfigurations
{
    public class SanctuaryConfiguration
    {
        public SanctuaryConfiguration(EntityTypeBuilder<Domain.Entities.Sanctuary> entity)
        {
            entity.ToTable("Sanctuary");

            entity.HasKey(k => k.Id);

            entity.Property(p => p.Name)
                .IsRequired()
                .HasColumnType("varchar(200)");

            entity.Property(p => p.FullAddress)
                .IsRequired()
                .HasColumnType("varchar(1000)");

            entity.Property(p => p.CreatedAt)
                .IsRequired();
        }
    }
}
