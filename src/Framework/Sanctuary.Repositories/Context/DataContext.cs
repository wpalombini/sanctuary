﻿using Microsoft.EntityFrameworkCore;
using Sanctuary.Domain.Entities;
using Sanctuary.Repositories.EntityConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Repositories.Context
{
    public class DataContext : DbContext
    {
        public DbSet<Account> Account { get; set; }

        public DbSet<Domain.Entities.Sanctuary> Sanctuary { get; set; }

        public DbSet<AccountSanctuary> AccountSanctuary { get; set; }

        public DbSet<AnimalSpecies> AnimalSpecies { get; set; }

        public DbSet<AnimalBreed> AnimalBreed { get; set; }

        public DbSet<Animal> Animal { get; set; }
        

        public DataContext(DbContextOptions dbOptions) : base(dbOptions)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            new AccountConfiguration(modelBuilder.Entity<Account>());

            new SanctuaryConfiguration(modelBuilder.Entity<Domain.Entities.Sanctuary>());

            new AccountSanctuaryConfiguration(modelBuilder.Entity<AccountSanctuary>());

            new AnimalSpeciesConfiguration(modelBuilder.Entity<AnimalSpecies>());

            new AnimalBreedConfiguration(modelBuilder.Entity<AnimalBreed>());

            new AnimalConfiguration(modelBuilder.Entity<Animal>());
        }
    }
}
