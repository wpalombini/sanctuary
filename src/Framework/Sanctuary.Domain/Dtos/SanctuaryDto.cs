﻿using Sanctuary.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Helpers;

namespace Sanctuary.Domain.Dtos
{
    public class SanctuaryDto : BaseDto, IDto<Entities.Sanctuary, SanctuaryDto>
    {
        public Guid Id { get; set; }

        public Guid AccountId { get; set; }

        public string Name { get; set; }

        public string FullAddress { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public SanctuaryDto Map(Entities.Sanctuary obj)
        {
            Id = obj.Id;
            Name = obj.Name;
            CreatedAt = obj.CreatedAt.ToUtcKind();
            DeletedAt = obj.DeletedAt.ToUtcKind();
            FullAddress = obj.FullAddress;

            return this;
        }
    }
}
