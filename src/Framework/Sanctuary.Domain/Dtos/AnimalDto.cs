﻿using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Helpers;
using Sanctuary.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Dtos
{
    public class AnimalDto : BaseDto, IDto<Animal, AnimalDto>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        public DateTime CreatedAt { get; set; }

        public int? Age { get; set; }

        public DateTime? DeletedAt { get; set; }

        public Guid AnimalBreedId { get; set; }

        public string AnimalBreedName { get; set; }

        public string AnimalSpeciesName { get; set; }

        public Guid SanctuaryId { get; set; }

        public AnimalDto Map(Animal obj)
        {
            Id = obj.Id;
            Name = obj.Name;
            Gender = obj.Gender;
            CreatedAt = obj.CreatedAt.ToUtcKind();
            Age = obj.DateOfBirth.HasValue
                ? DateTime.UtcNow.Year - obj.DateOfBirth.Value.Year
                : Age;
            DeletedAt = obj.DeletedAt.ToUtcKind();
            AnimalBreedId = obj.AnimalBreedId;
            SanctuaryId = obj.SanctuaryId;
            AnimalBreedName = obj.AnimalBreed?.Name;
            AnimalSpeciesName = obj.AnimalBreed?.AnimalSpecies?.Name;

            return this;
        }
    }
}
