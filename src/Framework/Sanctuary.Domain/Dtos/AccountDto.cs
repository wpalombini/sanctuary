﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Helpers;
using Sanctuary.Domain.Interfaces;

namespace Sanctuary.Domain.Dtos
{
    public class AccountDto : BaseDto, IDto<Account, AccountDto>
    {
        public Guid Id { get; set; }

        public string AuthenticationDetails { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public bool IsAdmin { get; set; }

        public AccountDto Map(Account obj)
        {
            this.Id = obj.Id;
            this.EmailAddress = obj.EmailAddress;
            this.FirstName = obj.FirstName;
            this.LastName = obj.LastName;
            this.IsAdmin = obj.IsAdmin;
            this.CreatedAt = obj.CreatedAt.ToUtcKind();
            this.DeletedAt = obj.DeletedAt.ToUtcKind();

            return this;
        }
    }
}
