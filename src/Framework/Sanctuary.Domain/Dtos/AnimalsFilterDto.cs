﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Dtos
{
    public class AnimalsFilterDto
    {
        public Guid AccountId { get; set; }

        public Guid SanctuaryId { get; set; }
        public Guid? AnimalSpeciesId { get; set; }

        public Guid? AnimalBreedId { get; set; }

        public string AnimalName { get; set; }

        public int ResultsPerPage { get; set; } = 5;

        public int Page { get; set; } = 1;
    }
}
