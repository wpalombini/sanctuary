﻿using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Dtos
{
    public class AnimalSpeciesDto : BaseDto, IDto<AnimalSpecies, AnimalSpeciesDto>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid? SanctuaryId { get; set; }

        public AnimalSpeciesDto Map(AnimalSpecies obj)
        {
            Id = obj.Id;
            Name = obj.Name;
            SanctuaryId = obj.SanctuaryId;

            return this;
        }
    }
}
