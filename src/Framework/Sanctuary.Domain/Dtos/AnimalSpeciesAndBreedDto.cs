﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Dtos
{
    public class AnimalSpeciesAndBreedDto : BaseDto
    {
        public IEnumerable<AnimalSpeciesDto> AnimalSpecies { get; set; } = new List<AnimalSpeciesDto>();
        public IEnumerable<AnimalBreedDto> AnimalBreeds { get; set; } = new List<AnimalBreedDto>();
    }
}
