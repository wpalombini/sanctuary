﻿using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Dtos
{
    public class AnimalBreedDto : BaseDto, IDto<AnimalBreed, AnimalBreedDto>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid AnimalSpeciesId { get; set; }

        public Guid? SanctuaryId { get; set; }

        public AnimalBreedDto Map(AnimalBreed obj)
        {
            Id = obj.Id;
            Name = obj.Name;
            AnimalSpeciesId = obj.AnimalSpeciesId;
            SanctuaryId = obj.SanctuaryId;

            return this;
        }
    }
}
