﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sanctuary.Domain.Interfaces;
using Sanctuary.Domain.Entities;

namespace Sanctuary.Domain.Dtos
{
    public class ContactDto : BaseDto, IDto<Contact, ContactDto>
    {
        public string EmailAddress { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }

        public ContactDto Map(Contact model)
        {
            this.EmailAddress = model.EmailAddress;
            this.Message = model.Message;
            this.Name = model.Name;

            return this;
        }
    }
}
