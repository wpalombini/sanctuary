﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Dtos
{
    public class ChangePasswordDto
    {
        public Guid AccountId { get; set; }

        public string Password { get; set; }

        public string ConfirmedPassword { get; set; }
    }
}
