﻿namespace Sanctuary.Domain.Interfaces
{
    public interface IMapper<TFrom, TTo>
    {
        TTo Map(TFrom obj);
    }
}
