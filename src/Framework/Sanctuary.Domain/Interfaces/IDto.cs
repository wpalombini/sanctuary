﻿using System;
using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Entities;

namespace Sanctuary.Domain.Interfaces
{
    public interface IDto<TEntity, TDto> : IMapper<TEntity, TDto> where TEntity : BaseEntity where TDto : BaseDto
    {

    }
}
