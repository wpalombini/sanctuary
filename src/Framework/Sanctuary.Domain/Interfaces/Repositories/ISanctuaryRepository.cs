﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Interfaces.Repositories
{
    public interface ISanctuaryRepository : IBaseRepository<Entities.Sanctuary>
    {
        Task<Entities.Sanctuary> GetBySanctuaryName(string name);

        Task<IEnumerable<Entities.Sanctuary>> GetSanctuariesForAccountId(Guid accountId);
        Task<Entities.Sanctuary> GetSanctuaryByIdForAccountId(Guid id, Guid accountId);
    }
}
