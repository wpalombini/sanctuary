﻿using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Interfaces.Repositories
{
    public interface IAccountRepository : IBaseRepository<Account>
    {
        Task<Account> GetActiveAccountByEmailAsync(string email);

        Task<Account> GetAccountByAuthenticationDetails(string provider, string authenticationId);
    }
}
