﻿using Sanctuary.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Interfaces.Repositories
{
    public interface IAuthenticationRepository
    {
        Task<bool> ChangePassword(ChangePasswordDto changePasswordDto);
    }
}
