﻿using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Interfaces.Repositories
{
    public interface IBaseRepository<T> where T : BaseDatabaseEntity
    {
        Task<IEnumerable<T>> GetAllWithNoTrackingAsync();
        Task<T> GetByIdAsync(Guid Id);
        Task AddAsync(T obj);
        Task UpdateAsync(T obj);
        Task<bool> DeleteAsync(Guid Id);
        Task<int> SaveAsync();
    }
}
