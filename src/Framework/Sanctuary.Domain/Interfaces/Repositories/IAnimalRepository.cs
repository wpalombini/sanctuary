﻿using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Interfaces.Repositories
{
    public interface IAnimalRepository : IBaseRepository<Animal>
    {
        Task<IEnumerable<AnimalSpecies>> GetAnimalSpecies();

        Task<IEnumerable<AnimalSpecies>> GetAnimalSpecies(Guid sanctuaryId);

        Task<IEnumerable<AnimalBreed>> GetAnimalBreeds();

        Task<IEnumerable<AnimalBreed>> GetAnimalBreeds(Guid sanctuaryId);

        Task<IEnumerable<Animal>> GetAnimalsByFilter(Dtos.AnimalsFilterDto filter);

        Task<Animal> GetAnimalById(Guid id);
    }
}
