﻿using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Entities;

namespace Sanctuary.Domain.Interfaces
{
    public interface IDtoMapper
    {
        TDto MapFrom<TEntity, TDto>(TEntity entity)
            where TEntity : BaseEntity where TDto : BaseDto, IDto<TEntity, TDto>, new();
    }
}
