﻿using Sanctuary.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Interfaces.Services
{
    public interface ISanctuaryService : IBaseService<Entities.Sanctuary, SanctuaryDto>
    {
        Task<SanctuaryDto> CreateSanctuary(SanctuaryDto newSanctuaryDto);

        Task<IEnumerable<SanctuaryDto>> GetSanctuariesForAccountId(Guid accountId);
        Task<SanctuaryDto> EditSanctuary(SanctuaryDto sanctuaryDto);
    }
}
