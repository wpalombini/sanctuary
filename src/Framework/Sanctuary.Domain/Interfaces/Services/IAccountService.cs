﻿using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Interfaces.Services
{
    public interface IAccountService : IBaseService<Account, AccountDto>
    {
        Task<AccountDto> CreateAccount(AccountDto newAccountDto);

        Task<bool> IsActiveAsync(Guid accountId);

        Task<Guid> GetAccountIdByAuthentication(string authenticationDetails);

        Task<AccountDto> GetAccountDetails(AccountDto accountDto);

        Task<AccountDto> EditContactDetails(AccountDto accountDto);
    }
}
