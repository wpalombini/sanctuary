﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Entities;

namespace Sanctuary.Domain.Interfaces.Services
{
    public interface IContactService
    {
        void SetRootPath(string webRootPath);
        Task Send(ContactDto contact);
    }
}
