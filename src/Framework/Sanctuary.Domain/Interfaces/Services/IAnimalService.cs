﻿using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Interfaces.Services
{
    public interface IAnimalService : IBaseService<Animal, AnimalDto>
    {
        Task<AnimalSpeciesAndBreedDto> GetSpeciesAndBreeds(SanctuaryDto sanctuaryDto);

        Task<AnimalDto> Create(AnimalDto animalDto);

        Task<IEnumerable<AnimalDto>> GetAnimals(AnimalsFilterDto filter);

        Task<AnimalDto> GetAnimalById(string id);
    }
}
