﻿using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Interfaces.Services
{
    public interface IBaseService<T, U> where T : BaseDatabaseEntity where U : BaseDto
    {
        Task<IEnumerable<U>> GetAll();
    }
}
