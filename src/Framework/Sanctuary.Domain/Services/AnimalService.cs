﻿using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Exceptions;
using Sanctuary.Domain.Interfaces;
using Sanctuary.Domain.Interfaces.Repositories;
using Sanctuary.Domain.Interfaces.Services;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Services
{
    public class AnimalService : BaseService<Animal, AnimalDto>, IAnimalService
    {
        private readonly IAnimalRepository _animalRepository;

        public AnimalService(IAnimalRepository animalRepository, IDtoMapper mapper)
            : base(animalRepository, mapper)
        {
            this._animalRepository = animalRepository;
        }

        public async Task<AnimalDto> Create(AnimalDto animalDto)
        {
            #region Validation

            if (animalDto.AnimalBreedId == Guid.Empty)
                throw new AnimalCreateException("Invalid Breed.");

            //if (animalDto.Id != Guid.Empty && (await this.AnimalExists(animalDto.Id))) //then the Guid is an existing one
            //    throw new AnimalCreateException("This animal has already been registered");

            if (string.IsNullOrEmpty(animalDto.Name))
                throw new AnimalCreateException("Invalid Name.");

            if (string.IsNullOrEmpty(animalDto.Gender) || animalDto.Gender.Length != 1)
                throw new AnimalCreateException("Invalid Gender.");

            if (animalDto.SanctuaryId == Guid.Empty)
                throw new AnimalCreateException("Invalid Sanctuary.");

            #endregion

            var animal = new Animal()
            {
                Name = animalDto.Name,
                AnimalBreedId = animalDto.AnimalBreedId,
                Gender = animalDto.Gender,
                SanctuaryId = animalDto.SanctuaryId,
                DateOfBirth = animalDto.Age.HasValue
                    ? new DateTime(DateTime.UtcNow.Year - animalDto.Age.Value, 1, 1)
                    : (DateTime?)null
            };

            await this._animalRepository.AddAsync(animal);

            await this._animalRepository.SaveAsync();

            return animalDto.Map(animal);
        }

        public async Task<AnimalSpeciesAndBreedDto> GetSpeciesAndBreeds(SanctuaryDto sanctuaryDto)
        {
            var speciesDto = new List<AnimalSpeciesDto>();
            var breedsDto = new List<AnimalBreedDto>();

            var species = await this._animalRepository.GetAnimalSpecies(sanctuaryDto.Id);
            var breeds = await this._animalRepository.GetAnimalBreeds(sanctuaryDto.Id);

            foreach (var s in species)
                speciesDto.Add(new AnimalSpeciesDto().Map(s));

            foreach (var b in breeds)
                breedsDto.Add(new AnimalBreedDto().Map(b));

            return new AnimalSpeciesAndBreedDto()
            {
                AnimalBreeds = breedsDto.OrderBy(o => o.Name),
                AnimalSpecies = speciesDto.OrderBy(o => o.Name)
            };
        }

        public async Task<IEnumerable<AnimalDto>> GetAnimals(AnimalsFilterDto filter)
        {
            #region Validation

            if (filter.AccountId == Guid.Empty)
                throw new AnimalFilterException("Invalid account.");

            #endregion

            var query = await this._animalRepository.GetAnimalsByFilter(filter);
            
            var animals = query.ToList();

            var result = new List<AnimalDto>();

            foreach (var animal in animals)
            {
                var animalDto = new AnimalDto().Map(animal);
                animalDto.AnimalBreedName = animal.AnimalBreed.Name;
                animalDto.AnimalSpeciesName = animal.AnimalBreed.AnimalSpecies.Name;
                result.Add(animalDto);
            }

            return result;
        }

        public async Task<AnimalDto> GetAnimalById(string id)
        {
            var animalId = Guid.Parse(id);
            var animal = await this._animalRepository.GetAnimalById(animalId);

            return new AnimalDto().Map(animal);
        }
    }
}
