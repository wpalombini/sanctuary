﻿using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Exceptions;
using Sanctuary.Domain.Interfaces;
using Sanctuary.Domain.Interfaces.Repositories;
using Sanctuary.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Services
{
    public class SanctuaryService : BaseService<Entities.Sanctuary, SanctuaryDto>, ISanctuaryService
    {
        private readonly ISanctuaryRepository _sanctuaryRepository;
        private readonly IAccountRepository _accountRepository;

        public SanctuaryService(
            IAccountRepository accountRepository,
            ISanctuaryRepository sanctuaryRepository,
            IDtoMapper mapper)
            : base(sanctuaryRepository, mapper)
        {
            this._sanctuaryRepository = sanctuaryRepository;
            this._accountRepository = accountRepository;
        }

        public async Task<SanctuaryDto> CreateSanctuary(SanctuaryDto newSanctuaryDto)
        {
            #region Validation
            if (string.IsNullOrEmpty(newSanctuaryDto.Name))
                throw new SanctuaryCreateException("Please enter the sanctuary name.");

            if (string.IsNullOrEmpty(newSanctuaryDto.FullAddress))
                throw new SanctuaryCreateException("Please enter the sanctuary address.");

            var sanctuary = await this._sanctuaryRepository.GetBySanctuaryName(newSanctuaryDto.Name);
            if (sanctuary != null)
                throw new SanctuaryCreateException("There is a sanctuary with this name on the system already.");
            #endregion

            var account = await this._accountRepository.GetByIdAsync(newSanctuaryDto.AccountId);

            sanctuary = new Entities.Sanctuary()
            {
                Id = Guid.NewGuid(),
                Name = newSanctuaryDto.Name,
                FullAddress = newSanctuaryDto.FullAddress
            };

            var accountSanctuary = new AccountSanctuary()
            {
                Account = account,
                Sanctuary = sanctuary
            };

            account.AccountSanctuaries.Add(accountSanctuary);

            await this._accountRepository.SaveAsync();

            return new SanctuaryDto().Map(sanctuary);
        }

        public async Task<SanctuaryDto> EditSanctuary(SanctuaryDto sanctuaryDto)
        {
            #region Validate
            if (sanctuaryDto.Id == Guid.Empty)
                throw new SanctuaryEditException("Invalid sanctuary Id.");

            if (string.IsNullOrEmpty(sanctuaryDto.Name))
                throw new SanctuaryEditException("Invalid Sanctuary name.");

            if (string.IsNullOrEmpty(sanctuaryDto.FullAddress))
                throw new SanctuaryEditException("Invalid Sanctuary address.");

            var sanctuary = await this._sanctuaryRepository
                .GetSanctuaryByIdForAccountId(sanctuaryDto.Id, sanctuaryDto.AccountId);

            if (sanctuary == null)
                throw new SanctuaryNotFoundException("Sanctuary not found.");
            #endregion

            //Update
            sanctuary.Name = sanctuaryDto.Name;
            sanctuary.FullAddress = sanctuaryDto.FullAddress;

            await this._sanctuaryRepository.UpdateAsync(sanctuary);
            await this._sanctuaryRepository.SaveAsync();

            return sanctuaryDto;
        }

        public async Task<IEnumerable<SanctuaryDto>> GetSanctuariesForAccountId(Guid accountId)
        {
            if (accountId == Guid.Empty)
                throw new AccountNotFoundException("Invalid AccountId.");

            var sanctuaries = await this._sanctuaryRepository.GetSanctuariesForAccountId(accountId);

            if (sanctuaries == null || sanctuaries.Any() == false)
                throw new SanctuaryNotFoundException("Sanctuary not found.");

            var result = new List<SanctuaryDto>();

            foreach (var sanctuary in sanctuaries)
            {
                result.Add(new SanctuaryDto().Map(sanctuary));
            }

            return result;
        }
    }
}
