﻿using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Exceptions;
using Sanctuary.Domain.Interfaces;
using Sanctuary.Domain.Interfaces.Repositories;
using Sanctuary.Domain.Interfaces.Services;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Services
{
    public class AccountService : BaseService<Account, AccountDto>, IAccountService
    {
        private readonly IAccountRepository _accountRepository;

        public AccountService(
            IAccountRepository accountRepository,
            IDtoMapper mapper)
            : base(accountRepository, mapper)
        {
            this._accountRepository = accountRepository;
        }

        public async Task<AccountDto> GetAccountDetails(AccountDto accountDto)
        {
            var account = await this._accountRepository.GetByIdAsync(accountDto.Id);

            var result = new AccountDto().Map(account);

            return result;
        }

        public async Task<AccountDto> EditContactDetails(AccountDto accountDto)
        {
            #region Validation
            if (string.IsNullOrEmpty(accountDto.FirstName))
                throw new AccountEditException("Invalid first name.");

            if (string.IsNullOrEmpty(accountDto.LastName))
                throw new AccountEditException("Invalid last name.");
            #endregion

            var account = await this._accountRepository.GetByIdAsync(accountDto.Id);

            account.FirstName = accountDto.FirstName;
            account.LastName = accountDto.LastName;

            await this._accountRepository.UpdateAsync(account);
            await this._accountRepository.SaveAsync();

            return accountDto.Map(account);
        }
        
        public async Task<bool> IsActiveAsync(Guid Id)
        {
            var account = await this._accountRepository.GetByIdAsync(Id);

            return account != null && account.DeletedAt.HasValue == false;
        }

        public async Task<AccountDto> CreateAccount(AccountDto newAccountDto)
        {
            #region Validation
            if (this.isValidEmail(newAccountDto.EmailAddress) == false)
            {
                throw new AccountCreateException("Invalid email format.");
            }
            if (await this.emailAddressIsInUse(newAccountDto.EmailAddress))
            {
                throw new AccountCreateException("This email address is already in use. Did you forget your password?");
            }
            #endregion

            var newAccount = new Account()
            {
                Id = Guid.NewGuid(),
                Provider = newAccountDto.AuthenticationDetails.Split('|')[0],
                AuthenticationId = newAccountDto.AuthenticationDetails.Split('|')[1],
                EmailAddress = newAccountDto.EmailAddress,
                IsAdmin = true
            };

            await _accountRepository.AddAsync(newAccount);
            await _accountRepository.SaveAsync();

            return new AccountDto().Map(newAccount);
        }
        
        public async Task<Guid> GetAccountIdByAuthentication(string authenticationDetails)
        {
            string provider = authenticationDetails.Split('|')[0];
            string authenticationId = authenticationDetails.Split('|')[1];
            
            var account = 
                await this._accountRepository.GetAccountByAuthenticationDetails(provider, authenticationId);

            if (account == null)
            {
                throw new AccountNotFoundException("Account not found or not active.");
            }

            return account.Id;
        }
        
        private bool isValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;

            return Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        private async Task<bool> emailAddressIsInUse(string email)
        {
            return await this._accountRepository.GetActiveAccountByEmailAsync(email) != null;
        }
    }
}
