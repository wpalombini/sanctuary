﻿using MailKit.Net.Smtp;
using MimeKit;
using System.Net;
using System.Threading.Tasks;
using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Helpers;
using Sanctuary.Domain.Interfaces.Services;

namespace Sanctuary.Domain.Services
{
    public class ContactService : IContactService
    {
        private string _webRootPath;

        public void SetRootPath(string webRootPath)
        {
            this._webRootPath = webRootPath;
        }

        public async Task Send(ContactDto contact)
        {
            MimeMessage email = new MimeMessage();
            email.From.Add(new MailboxAddress("Website", "wpalombini@wpalombini.com"));
            email.To.Add(new MailboxAddress("", "wpalombini@wpalombini.com"));
            email.Subject = "Message from WPalombini - Contact Page";
            email.Body = new TextPart("plain") { Text = contact.EmailAddress + "\n" + contact.Name + "\n" + contact.Message };

            using (var client = new SmtpClient())
            {
                var creds = SmtpCredentialsHelper.GetSmtpCredentials(_webRootPath);

                var credentials = new NetworkCredential()
                {
                    UserName = creds.Username,
                    Password = creds.Password
                };
                client.LocalDomain = "wpalombini.com";
                await client.ConnectAsync("smtp.zoho.com", 465, MailKit.Security.SecureSocketOptions.SslOnConnect);
                await client.AuthenticateAsync(credentials);
                await client.SendAsync(email).ConfigureAwait(false);
                await client.DisconnectAsync(true).ConfigureAwait(false);
            }
        }
    }
}
