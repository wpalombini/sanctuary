﻿using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Exceptions;
using Sanctuary.Domain.Interfaces.Repositories;
using Sanctuary.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IAuthenticationRepository _authenticationRepository;

        public AuthenticationService(IAuthenticationRepository authenticationRepository)
        {
            this._authenticationRepository = authenticationRepository;
        }

        public async Task<bool> ChangePassword(ChangePasswordDto changePasswordDto)
        {
            #region Validation
            if (changePasswordDto.Password.Length < 5)
            {
                throw new ChangePasswordException("Minimum of 6 characters or digits.");
            }
            else if (changePasswordDto.Password != changePasswordDto.ConfirmedPassword)
            {
                throw new ChangePasswordException("Passwords dont match.");
            }
            #endregion

            return await this._authenticationRepository.ChangePassword(changePasswordDto);
        }
    }
}
