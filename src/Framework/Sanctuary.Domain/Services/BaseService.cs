﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Interfaces;
using Sanctuary.Domain.Interfaces.Repositories;
using Sanctuary.Domain.Interfaces.Services;
using Sanctuary.Domain.Entities;

namespace Sanctuary.Domain.Services
{
    public abstract class BaseService<T, U> : IBaseService<T, U> where T : BaseDatabaseEntity where U : BaseDto, IDto<T, U>, new()
    {
        private IDtoMapper _mapper;
        private IBaseRepository<T> _baseRepository;

        public BaseService(IBaseRepository<T> baseRepository, IDtoMapper mapper)
        {
            _mapper = mapper;
            _baseRepository = baseRepository;
        }

        protected IDtoMapper Mapper => _mapper;

        public virtual async Task<IEnumerable<U>> GetAll()
        {
            return (await _baseRepository.GetAllWithNoTrackingAsync()).Select(s => _mapper.MapFrom<T, U>(s));
        }
    }
}
