﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Exceptions
{
    public class AnimalCreateException : BaseException
    {
        public AnimalCreateException(string message) : base(message) { }
    }

    public class AnimalFilterException : BaseException
    {
        public AnimalFilterException(string message) : base(message) { }
    }
}
