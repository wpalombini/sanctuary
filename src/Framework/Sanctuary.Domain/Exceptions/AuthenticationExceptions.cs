﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Exceptions
{
    public class ChangePasswordException : BaseException
    {
        public ChangePasswordException(string message) : base(message) { }
    }
}
