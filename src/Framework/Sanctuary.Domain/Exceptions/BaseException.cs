﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Exceptions
{
    public abstract class BaseException : Exception
    {
        public BaseException(string message) : base(message) { }
    }
}
