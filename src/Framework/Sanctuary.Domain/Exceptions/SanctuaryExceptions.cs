﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Exceptions
{
    public class SanctuaryNotFoundException : BaseException
    {
        public SanctuaryNotFoundException(string message) : base(message) { }
    }

    public class SanctuaryEditException : BaseException
    {
        public SanctuaryEditException(string message) : base(message) { }
    }

    public class SanctuaryCreateException : BaseException
    {
        public SanctuaryCreateException(string message) : base(message) { }
    }
}
