﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Exceptions
{
    public class AccountNotFoundException : BaseException
    {
        public AccountNotFoundException(string message) : base(message) { }
    }

    public class AccountEditException : BaseException
    {
        public AccountEditException(string message) : base(message) { }
    }

    public class AccountCreateException : BaseException
    {
        public AccountCreateException(string message) : base(message) { }
    }
}
