﻿using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Interfaces;

namespace Sanctuary.Domain.Mappers
{
    public class DtoMapper : IDtoMapper
    {
        public TDto MapFrom<TEntity, TDto>(TEntity entity)
            where TEntity : BaseEntity
            where TDto : BaseDto, IDto<TEntity, TDto>, new()
        {
            TDto dto = new TDto();
            return dto.Map(entity);
        }
    }
}
