﻿using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Entities
{
    public class AnimalSpecies : BaseDatabaseEntity
    {
        public string Name { get; set; }

        public Guid? SanctuaryId { get; set; }
        public virtual Sanctuary Sanctuary { get; set; }

        public ICollection<AnimalBreed> AnimalBreeds { get; set; } = new List<AnimalBreed>();
    }
}
