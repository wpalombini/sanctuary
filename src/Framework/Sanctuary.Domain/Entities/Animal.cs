﻿using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Entities
{
    public class Animal : BaseDatabaseEntity
    {
        public string Name { get; set; }

        public string Gender { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

        public DateTime? DeletedAt { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public Guid AnimalBreedId { get; set; }
        public virtual AnimalBreed AnimalBreed { get; set; }

        public Guid SanctuaryId { get; set; }
        public virtual Sanctuary Sanctuary { get; set; }
    }
}
