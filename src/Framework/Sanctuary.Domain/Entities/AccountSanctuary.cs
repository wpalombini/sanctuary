﻿using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Entities
{
    public class AccountSanctuary : BaseDatabaseEntity
    {
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        public Guid SanctuaryId { get; set; }
        public virtual Sanctuary Sanctuary { get; set; }
    }
}
