﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Entities
{
    public class Contact : BaseEntity
    {
        public string EmailAddress { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
    }
}
