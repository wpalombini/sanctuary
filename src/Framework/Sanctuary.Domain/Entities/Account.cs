﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Entities
{
    public class Account : BaseDatabaseEntity
    {
        public string Provider { get; set; }

        public string AuthenticationId { get; set; }

        public string EmailAddress { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool IsAdmin { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

        public DateTime? DeletedAt { get; set; }

        public ICollection<AccountSanctuary> AccountSanctuaries { get; set; } = new List<AccountSanctuary>();
    }
}
