﻿using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sanctuary.Domain.Entities
{
    public class AnimalBreed : BaseDatabaseEntity
    {
        public string Name { get; set; }

        public Guid AnimalSpeciesId { get; set; }
        public virtual AnimalSpecies AnimalSpecies { get; set; }

        public Guid? SanctuaryId { get; set; }
        public virtual Sanctuary Sanctuary { get; set; }

        public ICollection<Animal> Animals { get; set; } = new List<Animal>();
    }
}
