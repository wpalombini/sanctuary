﻿using Sanctuary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Entities
{
    public class Sanctuary : BaseDatabaseEntity
    {
        public string Name { get; set; }

        public string FullAddress { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

        public DateTime? DeletedAt { get; set; }

        public ICollection<AccountSanctuary> AccountSanctuaries { get; set; } = new List<AccountSanctuary>();

        public ICollection<AnimalSpecies> AnimalSpecies { get; set; } = new List<AnimalSpecies>();

        public ICollection<AnimalBreed> AnimalBreeds { get; set; } = new List<AnimalBreed>();

        public ICollection<Animal> Animals { get; set; } = new List<Animal>();
    }
}
