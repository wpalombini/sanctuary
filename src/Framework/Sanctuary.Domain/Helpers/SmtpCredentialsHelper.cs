﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Sanctuary.Domain.Helpers
{
    public class SmtpCredentialsHelper
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public static SmtpCredentialsHelper GetSmtpCredentials(string webRootPath)
        {
            /*
               creds.json should be sitting at the root of the website folder
               and should be in the following format:
                {
                  "smtp": {
                    "Username": "username here",
                    "Password": "password here"
                  }
                }
            */

            using (StreamReader sr = File.OpenText(webRootPath + "\\" + "creds.json"))
            {
                SmtpCredentialsHelper result = null;

                string strFile = sr.ReadToEnd();
                JObject jObj = JObject.Parse(strFile);

                if (jObj != null)
                {
                    result = jObj.SelectToken("smtp", false).ToObject<SmtpCredentialsHelper>();
                }

                return result ?? new SmtpCredentialsHelper();
            }
        }
    }
}
