﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Helpers
{
    public static class Global
    {
        public const string CustomAuthorization = "CustomAuthorization";

        public static readonly string ApplicationName = "Sanctuary";

        public static readonly string DefaultContactEmail = "wpalombini@wpalombini.com";

        public static readonly string Development = "Development";
        public static readonly string Production = "Production";

        public static readonly string DevHostnameUrl = "https://localhost:8080/";

        public static readonly string ProdHostnameUrl = "";
    }
}
