﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sanctuary.Domain.Helpers
{
    public static class ExtensionMethods
    {
        public static DateTime ToUtcKind(this DateTime dt)
        {
            return DateTime.SpecifyKind(dt, DateTimeKind.Utc);
        }

        public static DateTime? ToUtcKind(this DateTime? dt)
        {
            if (dt.HasValue)
            {
                return DateTime.SpecifyKind(dt.Value, DateTimeKind.Utc);
            }
            else
            {
                return dt;
            }
        }
    }
}
