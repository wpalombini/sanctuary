﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Sanctuary.Domain.Interfaces;
using Sanctuary.Domain.Interfaces.Repositories;
using Sanctuary.Domain.Interfaces.Services;
using Sanctuary.Domain.Mappers;
using Sanctuary.Domain.Services;
using Sanctuary.Repositories.Context;

namespace Sanctuary.IoC
{
    public class AutoFacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register<DataContext>(c =>
            {
                var config = c.Resolve<IConfiguration>();
                var opt = new DbContextOptionsBuilder<DataContext>();
                //opt.UseSqlServer(config.GetSection("ConnectionStrings:MyConnection:ConnectionString").Value);
                opt.UseSqlServer(@"Server=.\SQLEXPRESS;Database=SanctuaryDB;Trusted_Connection=True;");
                return new DataContext(opt.Options);
            }).AsSelf().InstancePerLifetimeScope();

            //Repositories
            builder.RegisterGeneric(typeof(Repositories.Repos.BaseRepository<>)).As(typeof(IBaseRepository<>)).InstancePerDependency();
            builder.RegisterType<Repositories.Repos.AccountRepository>().As<IAccountRepository>();
            builder.RegisterType<Repositories.Repos.SanctuaryRepository>().As<ISanctuaryRepository>();
            builder.RegisterType<Repositories.Repos.AnimalRepository>().As<IAnimalRepository>();
            builder.RegisterType<Repositories.Repos.AuthenticationRepository>().As<IAuthenticationRepository>();

            //Services
            builder.RegisterGeneric(typeof(BaseService<,>)).As(typeof(IBaseService<,>)).InstancePerDependency();
            builder.RegisterType<ContactService>().As<IContactService>();
            builder.RegisterType<AccountService>().As<IAccountService>();
            builder.RegisterType<SanctuaryService>().As<ISanctuaryService>();
            builder.RegisterType<AnimalService>().As<IAnimalService>();
            builder.RegisterType<AuthenticationService>().As<IAuthenticationService>();

            //Dtos
            builder.RegisterType<DtoMapper>().As<IDtoMapper>();

            base.Load(builder);
        }
    }
}
