﻿using Moq;
using Sanctuary.Domain.Dtos;
using Sanctuary.Domain.Entities;
using Sanctuary.Domain.Exceptions;
using Sanctuary.Domain.Interfaces;
using Sanctuary.Domain.Interfaces.Repositories;
using Sanctuary.Domain.Interfaces.Services;
using Sanctuary.Domain.Mappers;
using Sanctuary.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Sanctuary.UnitTests.Domain.Services
{
    public class AccountServiceTests
    {
        private Mock<IAccountRepository> _mockAccountRepo;
        private IAccountService _accountService;

        public AccountServiceTests()
        {
            this._mockAccountRepo = new Mock<IAccountRepository>(MockBehavior.Strict);
            this._accountService = new AccountService(this._mockAccountRepo.Object, new DtoMapper());
        }

        [Fact]
        public async Task Returns_True_When_Account_IsActive()
        {
            //Arrange
            var id = Guid.NewGuid();
            var account = new Account()
            {
                Id = id,
                DeletedAt = null
            };

            this._mockAccountRepo
                .Setup(repo => repo.GetByIdAsync(id))
                .Returns(Task.FromResult(account));

            //Act
            var result = await this._accountService.IsActiveAsync(id);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Returns_False_When_Account_Is_Not_Active()
        {
            //Arrange
            var id = Guid.NewGuid();
            var account = new Account()
            {
                Id = id,
                DeletedAt = new DateTime()
            };

            this._mockAccountRepo
                .Setup(repo => repo.GetByIdAsync(id))
                .Returns(Task.FromResult(account));

            //Act
            var result = await this._accountService.IsActiveAsync(id);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Returns_False_When_Account_Does_Not_Exist()
        {
            //Arrange
            var id = Guid.NewGuid();
            Account account = null;

            this._mockAccountRepo
                .Setup(repo => repo.GetByIdAsync(id))
                .Returns(Task.FromResult(account));

            //Act
            var result = await this._accountService.IsActiveAsync(id);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Returns_AccountDto_When_Account_Exists()
        {
            //Arrange
            var id = Guid.NewGuid();
            var account = new Account()
            {
                Id = id,
                FirstName = "first",
                LastName = "last",
                EmailAddress = "first.last@test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };

            var accountDto = new AccountDto()
            {
                Id = id,
                FirstName = "first",
                LastName = "last",
                EmailAddress = "first.last@test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };

            this._mockAccountRepo
                .Setup(repo => repo.GetByIdAsync(id))
                .Returns(Task.FromResult(account));

            //Act
            var result = await this._accountService.GetAccountDetails(accountDto);

            //Assert
            Assert.Equal(accountDto.Id, result.Id);
            Assert.Equal(accountDto.FirstName, result.FirstName);
            Assert.Equal(accountDto.LastName, result.LastName);
            Assert.Equal(accountDto.EmailAddress, result.EmailAddress);
            Assert.Equal(accountDto.CreatedAt, result.CreatedAt);
            Assert.Equal(accountDto.IsAdmin, result.IsAdmin);
        }

        [Fact]
        public async Task Returns_Updated_Account_Details_When_Editing_Account()
        {
            //Arrange
            var id = Guid.NewGuid();
            var account = new Account()
            {
                Id = id,
                FirstName = "first",
                LastName = "last",
                EmailAddress = "first.last@test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };

            var accountDto = new AccountDto()
            {
                Id = id,
                FirstName = "first-updated",
                LastName = "last-updated",
                EmailAddress = "first.last-updated@test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };

            this._mockAccountRepo
                .Setup(repo => repo.GetByIdAsync(accountDto.Id))
                .Returns(Task.FromResult(account));
            this._mockAccountRepo
                .Setup(repo => repo.UpdateAsync(account))
                .Returns(Task.CompletedTask);
            this._mockAccountRepo
                .Setup(repo => repo.SaveAsync())
                .Returns(Task.FromResult(1));

            //Act
            var result = await this._accountService.EditContactDetails(accountDto);

            //Assert
            Assert.Equal(accountDto.Id, result.Id);
            Assert.Equal(accountDto.FirstName, result.FirstName);
            Assert.Equal(accountDto.LastName, result.LastName);
            Assert.Equal(accountDto.EmailAddress, result.EmailAddress);
            Assert.Equal(accountDto.CreatedAt, result.CreatedAt);
            Assert.Equal(accountDto.IsAdmin, result.IsAdmin);
            //Ensure that all mocked methods were called by the service function.
            this._mockAccountRepo.VerifyAll();
        }

        [Fact]
        public async Task Returns_Error_When_No_Firstname_Is_Provided_When_Editing_Account()
        {
            //Arrange
            var id = Guid.NewGuid();

            var accountDto = new AccountDto()
            {
                Id = id,
                FirstName = "",
                LastName = "last",
                EmailAddress = "first.last-updated@test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };

            //Act
            var exception =
                await Assert.ThrowsAsync<AccountEditException>(async () =>
                    await this._accountService.EditContactDetails(accountDto));

            //Assert
            Assert.Equal("Invalid first name.", exception.Message);
        }

        [Fact]
        public async Task Returns_Error_When_No_Lastname_Is_Provided_When_Editing_Account()
        {
            //Arrange
            var id = Guid.NewGuid();

            var accountDto = new AccountDto()
            {
                Id = id,
                FirstName = "first",
                LastName = "",
                EmailAddress = "first.last-updated@test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };

            //Act
            var exception =
                await Assert.ThrowsAsync<AccountEditException>(async () =>
                    await this._accountService.EditContactDetails(accountDto));

            //Assert
            Assert.Equal("Invalid last name.", exception.Message);
        }

        [Fact]
        public async Task Returns_New_AccountDto_When_Account_Is_Created()
        {
            //Arrange
            var id = Guid.NewGuid();
            var account = new Account()
            {
                Id = id,
                EmailAddress = "first.last@test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };

            var accountDto = new AccountDto()
            {
                Id = id,
                AuthenticationDetails = "auth0|12345",
                EmailAddress = "first.last@test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };

            this._mockAccountRepo
                .Setup(repo => repo.AddAsync(It.IsAny<Account>()))
                .Returns(Task.CompletedTask);
            this._mockAccountRepo
                .Setup(repo => repo.SaveAsync())
                .Returns(Task.FromResult(1));
            this._mockAccountRepo
                .Setup(repo => repo.GetActiveAccountByEmailAsync(account.EmailAddress))
                .Returns(Task.FromResult<Account>(null));

            //Act
            var result = await this._accountService.CreateAccount(accountDto);

            //Assert
            
            Assert.True(result.Id != Guid.Empty);
            Assert.True(result.CreatedAt != DateTime.MinValue);
            Assert.True(accountDto.DeletedAt.HasValue == false);
            Assert.Equal(accountDto.EmailAddress, result.EmailAddress);
            Assert.Equal(accountDto.IsAdmin, result.IsAdmin);
        }

        [Fact]
        public async Task Returns_Error_When_Email_Is_Invalid_When_Creating_Account()
        {
            //Arrange
            var id = Guid.NewGuid();

            var accountDto = new AccountDto()
            {
                Id = id,
                FirstName = "first",
                LastName = "last",
                EmailAddress = "first.last-updated-test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };

            //Act
            var exception =
                await Assert.ThrowsAsync<AccountCreateException>(async () =>
                    await this._accountService.CreateAccount(accountDto));

            //Assert
            Assert.Equal("Invalid email format.", exception.Message);
        }

        [Fact]
        public async Task Returns_Error_When_Email_Is_Already_In_Use_When_Creating_Account()
        {
            //Arrange
            var id = Guid.NewGuid();

            var account = new Account()
            {
                Id = id,
                FirstName = "first",
                LastName = "last",
                EmailAddress = "first.last@test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };

            var accountDto = new AccountDto()
            {
                Id = id,
                FirstName = "first",
                LastName = "last",
                EmailAddress = "first.last@test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };

            //Act
            this._mockAccountRepo
                .Setup(repo => repo.GetActiveAccountByEmailAsync(accountDto.EmailAddress))
                .Returns(Task.FromResult<Account>(account));

            var exception =
                await Assert.ThrowsAsync<AccountCreateException>(async () =>
                    await this._accountService.CreateAccount(accountDto));

            //Assert
            Assert.Equal("This email address is already in use. Did you forget your password?", exception.Message);
        }

        [Fact]
        public async Task Returns_AccountId_When_AuthenticationId_Is_Provided()
        {
            //Arrange
            var id = Guid.NewGuid();
            var account = new Account()
            {
                Id = id,
                EmailAddress = "first.last@test.net",
                CreatedAt = new DateTime(),
                IsAdmin = true
            };
            string provider = "auth0";
            string authenticationId = "12345";
            string authenticationDetails = $"{provider}|{authenticationId}";

            this._mockAccountRepo
                .Setup(repo => repo.GetAccountByAuthenticationDetails(provider, authenticationId))
                .Returns(Task.FromResult(account));

            //Act
            var result = await this._accountService.GetAccountIdByAuthentication(authenticationDetails);

            //Assert
            Assert.Equal(result, id);
        }

        [Fact]
        public async Task Returns_Error_When_Account_Is_Not_Found_By_Authentication_Details()
        {
            //Arrange
            string provider = "auth0";
            string authenticationId = "12345";
            string authenticationDetails = $"{provider}|{authenticationId}";

            //Act
            this._mockAccountRepo
                .Setup(repo => repo.GetAccountByAuthenticationDetails(provider, authenticationId))
                .Returns(Task.FromResult((Account)null));

            var exception =
                await Assert.ThrowsAsync<AccountNotFoundException>(async () =>
                    await this._accountService.GetAccountIdByAuthentication(authenticationDetails));

            //Assert
            Assert.Equal("Account not found or not active.", exception.Message);
        }
    }
}
